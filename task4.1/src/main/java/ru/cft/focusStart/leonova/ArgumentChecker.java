package ru.cft.focusStart.leonova;

public class ArgumentChecker {
    private static final int FROM = 1;
    private static final int TO = 2000000000;

    public int parseAndCheck(String[] value){
        int result ;
        try{
            if(value.length != 1){
                throw new IllegalArgumentException("Bad quantity of arguments. Write only one number from " + FROM + " to " + TO);
            }

            result = Integer.parseInt(value[0]);
            if(result < FROM || result > TO){
                throw new IllegalArgumentException("Bad number: must be from " + FROM + " to " + TO);
            }
        }catch (NumberFormatException e){
            throw new IllegalArgumentException("Not a integer");
        }
        return result;
    }

}
