package ru.cft.focusStart.leonova;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

public class Task implements Callable<Double> {
    private static final Logger LOGGER = LoggerFactory.getLogger(Task.class);


    private TaskData data;

    public Task(TaskData data) {
        this.data = data;
    }

    @Override
    public Double call() {
        return countSubSum();
    }

    private double countSubSum(){

        LOGGER.info("Start: {}\nEnd: {}", data.getStart(), data.getEnd() );

        int sign;
        double subsum = 0;

        //Leibniz' formula of PI/4 counting
        for(int i = data.getStart(); i < data.getEnd(); ++i){
            sign = (i % 2 == 0) ? 1 : -1;
            subsum += sign / (2.0 * i + 1.0);
        }

        LOGGER.info("Subsum: {}", subsum);
        return subsum;
    }
}
