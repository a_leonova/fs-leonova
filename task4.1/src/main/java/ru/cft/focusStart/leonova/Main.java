package ru.cft.focusStart.leonova;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    private static final int TASKS_NUMBER = 10;
    private static final int THREAD_NUMBER = 4;

    public static void main(String[] args) {
        ArgumentChecker argumentChecker = new ArgumentChecker();
        int accuracy = argumentChecker.parseAndCheck(args);
        new Main().go(accuracy);
    }

    public void go(int accuracy){
        ArrayList<TaskData> dataForThreads = new ArrayList<>(TASKS_NUMBER);
        ArrayList<Future<Double>> subsums = new ArrayList<>(TASKS_NUMBER);
        ExecutorService pool = Executors.newFixedThreadPool(THREAD_NUMBER);

        fillDataForThreads(dataForThreads, accuracy);
        multiCountingPISubsum(dataForThreads, pool, subsums);
        countPIAndWrite(subsums);
    }

    private void fillDataForThreads(ArrayList<TaskData> dataForThreads, int accuracy){

        int mainAddend = accuracy / TASKS_NUMBER;
        int remainder = accuracy % TASKS_NUMBER;

        for (int i = 0; i < TASKS_NUMBER; ++i){
            int start = i * mainAddend;
            int end = start + mainAddend;
            if(i + 1 == TASKS_NUMBER){
                end += remainder;
            }
            dataForThreads.add(new TaskData(start, end));
        }
    }

    private void multiCountingPISubsum(ArrayList<TaskData> data, ExecutorService pool,
                                              ArrayList<Future<Double>> subsums){
        for(int i = 0; i < TASKS_NUMBER; ++i){
            Task task = new Task(data.get(i));
            subsums.add(pool.submit(task));
        }
        pool.shutdown();
    }

    private void countPIAndWrite(ArrayList<Future<Double>> subsums){
        double PI = 0;
        for(int i = 0; i < TASKS_NUMBER; ++i){
            try {
                PI += subsums.get(i).get();
            } catch (InterruptedException | ExecutionException e) {
                System.out.println(e.getMessage());
            }
        }
        LOGGER.info("PI from me   = {}", PI * 4);
        LOGGER.info("PI from Math = {}", Math.PI);
    }
}
