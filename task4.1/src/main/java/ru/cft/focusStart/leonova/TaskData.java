package ru.cft.focusStart.leonova;

public class TaskData{
    private int start;
    private int end;

    public TaskData(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }
}
