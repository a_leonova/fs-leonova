package ru.cft.focusStar.leonova.model;

import ru.cft.focusStar.leonova.PositionCellInnerState;
import ru.cft.focusStar.leonova.Position;
import ru.cft.focusStar.leonova.PositionCellOuterState;
import ru.cft.focusStar.leonova.gameObserver.GameObserver;
import ru.cft.focusStar.leonova.gameOptions.GameOptions;
import ru.cft.focusStar.leonova.model.cell.CellInnerState;
import ru.cft.focusStar.leonova.model.cell.CellOuterState;
import ru.cft.focusStar.leonova.model.events.*;
import ru.cft.focusStar.leonova.model.record.Record;

import java.util.ArrayList;
import java.util.List;

public class Notifier {

    private ArrayList<GameObserver> observers = new ArrayList<>();


    public void openCells(List<PositionCellInnerState> openedCells){
        notifyObservers(new InnerStateChangeEvent(openedCells));
    }

    public void outerStateChanged(List<PositionCellOuterState> changedOuterState) {
        notifyObservers(new OuterStateChangeEvent(changedOuterState));
    }

    public void loseGame(){
        notifyObservers(new LoseEvent());
    }

    public void createNewField(GameOptions gameOptions){
        notifyObservers(new FieldCreationEvent(gameOptions));
    }

    public void win(){
        notifyObservers(new WinEvent());
    }

    public void showRecords(List<Record> records){
        notifyObservers(new RecordsShowEvent(records));
    }

    public void fillNameWhenNewRecord(Record record){
        notifyObservers(new NewRecordEvent(record));
    }

    public void newCountFlaggedPlace(int count){
        notifyObservers(new FlaggedPlacesCountChangeEvent(count));
    }
    public void changeTime(int time){
        notifyObservers(new TimeCountChangeEvent(time));
    }

    public void addObserver(GameObserver obs) {
        observers.add(obs);
    }
    public void deleteObserver(GameObserver obs) {
        observers.remove(obs);
    }


    private void notifyObservers(EventAcceptor eventAcceptor){
        for(GameObserver observer : observers){
            observer.update(eventAcceptor);
        }
    }

}
