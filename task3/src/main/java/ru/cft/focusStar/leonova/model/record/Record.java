package ru.cft.focusStar.leonova.model.record;

import ru.cft.focusStar.leonova.gameOptions.GameLevel;

public class Record implements Comparable<Record>{

    public static final String SPLITTER = ":";

    private GameLevel level;
    private String name;
    private int time;

    public Record(String string){
        String[] comps = string.split(SPLITTER);
        if(comps.length != 3){
            throw new IllegalArgumentException("Could't parse line");
        }

        try{
            level = GameLevel.valueOf(comps[0]);
        }
        catch (IllegalArgumentException e){
            throw new IllegalArgumentException ("Bad level name");
        }
        name = comps[1];
        time = Integer.parseInt(comps[2]);
    }

    public Record(GameLevel level, int time){
        this.level = level;
        this.time = time;
    }

    public GameLevel getLevel() {
        return level;
    }

    public String getName() {
        return name;
    }

    public long getTime() {
        return time;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Record o) {
        return Integer.compare(this.time, o.time);
    }
}
