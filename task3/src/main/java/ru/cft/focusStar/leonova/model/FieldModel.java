package ru.cft.focusStar.leonova.model;

import ru.cft.focusStar.leonova.gameOptions.GameOptions;
import ru.cft.focusStar.leonova.Position;

public interface FieldModel {
    void openCell(Position position);
    void openCellNeighbours(Position position);
    void createNewGame(GameOptions gameOptions);
    void initializeField(Position position);
    void outerStateChanged(Position positions);
}
