package ru.cft.focusStar.leonova.model.field;

import ru.cft.focusStar.leonova.PositionCellOuterState;
import ru.cft.focusStar.leonova.model.NeighboursIndexes;
import ru.cft.focusStar.leonova.gameOptions.GameOptions;
import ru.cft.focusStar.leonova.PositionCellInnerState;
import ru.cft.focusStar.leonova.Position;
import ru.cft.focusStar.leonova.model.GameState;
import ru.cft.focusStar.leonova.model.OpeningResult;
import ru.cft.focusStar.leonova.model.cell.Cell;
import ru.cft.focusStar.leonova.model.cell.CellInnerState;
import ru.cft.focusStar.leonova.model.cell.CellOuterState;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class UsualField implements Field{
    private int width;
    private int height;
    private int bombNumber;
    private int notFlaggedPlacesCount;

    private ArrayList<Cell> field;

    @Override
    public void createNewField(GameOptions gameOptions) {
        this.width = gameOptions.getWidth();
        this.height = gameOptions.getHeight();
        bombNumber = gameOptions.getMineCount();
        notFlaggedPlacesCount = bombNumber;

        field = new ArrayList<>(width * height);
        for(int i = 0; i < width * height; ++i){
            field.add(new Cell());
        }
    }

    @Override
    public OpeningResult openCell(List<Position> positions) {
        OpeningResult openingResult = new OpeningResult();

        List<Position> cellsForOpening = findCellForOpening(positions);
        boolean openBomb = openAndCheckBomb(cellsForOpening);
        if(openBomb){
            findNotOpenedPlacesWithBomb(cellsForOpening);
            openingResult.setGameState(GameState.END_LOSE);
        }
        else if(allCellsWithoutBombOpened()){
            List<Position> flaggedPlaces = flagBombs();
            openingResult.setGameState(GameState.END_WIN);
            openingResult.setChangedOuterStateCells(createPairsPositionOuterState(flaggedPlaces));
        }
        openingResult.setOpenedCells(createPairsPositionInnerState(cellsForOpening));
        return openingResult;
    }

    public void changeOuterStateForNext(Position position){
        Cell cell = getCell(position);
        if(cell.getOuterState() != CellOuterState.OPEN){
            if(cell.getOuterState() == CellOuterState.FLAGGED){
                ++notFlaggedPlacesCount;
                cell.setOuterState(CellOuterState.QUESTION_MARK);
            }
            else if (cell.getOuterState() == CellOuterState.CLOSED){
                --notFlaggedPlacesCount;
                cell.setOuterState(CellOuterState.FLAGGED);
            }
            else if(cell.getOuterState() == CellOuterState.QUESTION_MARK){
                cell.setOuterState(CellOuterState.CLOSED);
            }
        }
    }

    public OpeningResult openCellAndNeighbours(Position position){
        Predicate<Cell> flaggedPredicate = cell -> cell.getOuterState() == CellOuterState.FLAGGED;
        Predicate<Cell> notFlaggedPredicate = cell -> cell.getOuterState() != CellOuterState.FLAGGED;

        OpeningResult result = new OpeningResult();

        int count[] = new int[1];
        iterateNeighbours(position, p -> count[0]++, flaggedPredicate);

        if(getCell(position).getNeighboursWithBomb() == count[0]){
            List<Position> positions = new ArrayList<>();
            iterateNeighbours(position, positions::add, notFlaggedPredicate);
            result = openCell(positions);
        }
        return result;
    }

    public void initializeField(Position startPosition) {
        fillBombs(startPosition);
        fillNotBombedCells();
    }

    @Override
    public int getNotFlaggedPlaces() {
        return notFlaggedPlacesCount;
    }

    private void iterateNeighbours(Position position, Consumer<Position> positionConsumer, Predicate<Cell> predicate){
        NeighboursIndexes neighboursIndexes = countNeighboursIndexes(position);
        for(int x = neighboursIndexes.getxBegin(); x <= neighboursIndexes.getxEnd(); ++x){
            for(int y = neighboursIndexes.getyBegin(); y <= neighboursIndexes.getyEnd(); ++y){
                Position neighbourPosition = new Position(x, y);
                Cell neighbourCell = getCell(neighbourPosition);
                if(predicate.test(neighbourCell) && !neighbourPosition.equals(position)){
                    positionConsumer.accept(neighbourPosition);
                }
            }
        }
    }

    private void fillBombs(Position startPosition){
        int filledBombs = 0;

        while (filledBombs != bombNumber){
            int randomX = (int)(Math.random() * width);
            int randomY = (int)(Math.random() * height);
            Position randomPosition = new Position(randomX, randomY);

            if(randomPosition.equals(startPosition) || getCell(randomPosition).getInnerState() == CellInnerState.BOMB){
                continue;
            }
            Cell cell = getCell(randomPosition);
            cell.setBomb(true);
            ++filledBombs;
        }
    }

    private void fillNotBombedCells(){
        for(int i = 0; i < width; ++i){
            for(int j = 0; j < height; ++j){
                Position position = new Position(i, j);
                Cell currentCell =  getCell(position);
                int countBombs[] = new int[1];
                iterateNeighbours(position, pos -> countBombs[0]++, cell -> cell.getInnerState() == CellInnerState.BOMB );
                currentCell.setCountNeighboursWithBomb(countBombs[0]);
            }
        }
    }

    public Cell getCell(Position position){
        return field.get(position.getY() * width + position.getX());
    }

    private List<Position> findCellForOpening(List<Position> position){
        List<Position> cellsForOpening = new ArrayList<>();
        Queue<Position> cellsForChecking = new LinkedList<>(position);

        while (!cellsForChecking.isEmpty()){
            Position cellPosition = cellsForChecking.poll();
            cellsForOpening.add(cellPosition);
            Cell cell = getCell(cellPosition);
            if(cell.getInnerState() == CellInnerState.EMPTY){
                addNeighbours(cellPosition, cellsForChecking, cellsForOpening);
            }
        }
        return cellsForOpening;
    }

    private void addNeighbours(Position position, Queue<Position> neighbours,
                               List<Position> addedCells){
        NeighboursIndexes neighboursIndexes = countNeighboursIndexes(position);

        for(int x = neighboursIndexes.getxBegin(); x <= neighboursIndexes.getxEnd(); ++x){
            for(int y = neighboursIndexes.getyBegin(); y <= neighboursIndexes.getyEnd(); ++y){
                Position neighbourPosition = new Position(x, y);
                if(!addedCells.contains(neighbourPosition) && !neighbours.contains(neighbourPosition)){
                    neighbours.add(neighbourPosition);
                }
            }
        }
    }

    private NeighboursIndexes countNeighboursIndexes(Position position){
        int xBegin = position.getX() - 1 < 0 ? position.getX() : position.getX() - 1;
        int xEnd = position.getX() + 1 >= width ? position.getX() : position.getX() + 1;
        int yBegin = position.getY() - 1 < 0 ? position.getY() : position.getY() - 1;
        int yEnd = position.getY() + 1 >= height ? position.getY() : position.getY() + 1;
        return new NeighboursIndexes(xBegin, xEnd, yBegin, yEnd);
    }

    private boolean openAndCheckBomb(List<Position> cellsForOpening){
        boolean openedBomb = false;
        for(Position position : cellsForOpening){
            Cell openingCell = getCell(position);
            openingCell.openCell();
            if(openingCell.getInnerState() == CellInnerState.BOMB){
                openingCell.explodedBomb();
                openedBomb = true;
            }
        }
        return openedBomb;
    }

    private void findNotOpenedPlacesWithBomb(List<Position> openingCells){
        for(int i = 0; i < width; ++i){
            for(int j = 0; j < height; ++j){
                Position position = new Position(i, j);
                Cell cell = getCell(position);
                if(cell.getInnerState() == CellInnerState.BOMB &&
                        cell.getOuterState() != CellOuterState.FLAGGED){
                    cell.openCell();
                    openingCells.add(position);
                }
            }
        }
    }

    private boolean allCellsWithoutBombOpened(){
        for(int i = 0; i < width; ++i){
            for(int j = 0; j < height; ++j){
                Position position = new Position(i, j);
                Cell cell = getCell(position);
                if(cell.getInnerState() != CellInnerState.BOMB && cell.getOuterState() != CellOuterState.OPEN){
                    return false;
                }
            }
        }
        return true;
    }

    private List<Position> flagBombs(){
        List<Position> flaggedPlaces = new ArrayList<>();

        for(int i = 0; i < width; ++i){
            for(int j = 0; j < height; ++j){
                Position position = new Position(i, j);
                Cell cell = getCell(position);
                if(cell.getInnerState() == CellInnerState.BOMB && cell.getOuterState() != CellOuterState.FLAGGED){
                    cell.setOuterState(CellOuterState.FLAGGED);
                    flaggedPlaces.add(position);
                }
            }
        }
        return flaggedPlaces;
    }


    private List<PositionCellInnerState> createPairsPositionInnerState(List<Position> positions) {
        List<PositionCellInnerState> newCellsInnerState = new ArrayList<>(positions.size());
        for(Position position : positions){
            newCellsInnerState.add(new PositionCellInnerState(position, getCell(position).getInnerState()));
        }
        return newCellsInnerState;
    }


    public List<PositionCellOuterState> createPairsPositionOuterState(List<Position> positions) {
        List<PositionCellOuterState> newCellsOuterState = new ArrayList<>(positions.size());
        for(Position position : positions){
            newCellsOuterState.add(new PositionCellOuterState(position, getCell(position).getOuterState()));
        }
        return newCellsOuterState;
    }

}
