package ru.cft.focusStar.leonova.model.events;

public class TimeCountChangeEvent implements EventAcceptor{
    private int time;

    public TimeCountChangeEvent(int time) {
        this.time = time;
    }

    public int getTime() {
        return time;
    }

    @Override
    public void accept(EventVisitor v) {
        v.visit(this);
    }
}
