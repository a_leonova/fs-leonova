package ru.cft.focusStar.leonova.model.record;

import ru.cft.focusStar.leonova.gameOptions.GameLevel;
import ru.cft.focusStar.leonova.model.record.Record;

import java.io.IOException;
import java.util.List;

public interface RecordsProvider {
    void saveRecords() throws IOException;
    void showRecords(GameLevel level);
}
