package ru.cft.focusStar.leonova.view.menus;

import ru.cft.focusStar.leonova.controller.RecordController;
import ru.cft.focusStar.leonova.controller.listener.GameInfoListener;
import ru.cft.focusStar.leonova.gameOptions.GameLevel;
import ru.cft.focusStar.leonova.gameOptions.GameOptions;
import ru.cft.focusStar.leonova.view.windows.ManualSettingWindow;

import javax.swing.*;

public class GameMenu {

    private GameInfoListener gameInfoListener;
    private RecordController recordListener;
    private JMenuBar menuBar;

    public GameMenu() {
        createMenu();
    }

    public void setGameInfoListener(GameInfoListener gameInfoListener) {
        this.gameInfoListener = gameInfoListener;
    }

    public void setRecordListener(RecordController recordListener) {
        this.recordListener = recordListener;
    }

    private void createMenu(){
        menuBar = new JMenuBar();

        JMenu settings = new JMenu("Settings");

        JMenuItem easyLevel = new JMenuItem("Easy level (9x9)");
        JMenuItem middleLevel = new JMenuItem("Middle level (16x16)");
        JMenuItem hardLevel = new JMenuItem("Hard level (30x16)");

        JMenuItem manualSettings = new JMenuItem("Manual settings");
        easyLevel.addActionListener(e -> gameInfoListener.createNewGame(GameOptions.EASY));
        middleLevel.addActionListener(e -> gameInfoListener.createNewGame(GameOptions.MIDDLE));
        hardLevel.addActionListener(e -> gameInfoListener.createNewGame(GameOptions.HARD));
        manualSettings.addActionListener(e -> new ManualSettingWindow(gameInfoListener).show());

        settings.add(easyLevel);
        settings.add(middleLevel);
        settings.add(hardLevel);
        settings.add(manualSettings);

        JMenu records = new JMenu("Records");

        JMenuItem easyRecords = new JMenuItem("Easy");
        JMenuItem middleRecords = new JMenuItem("Middle");
        JMenuItem hardRecords = new JMenuItem("Hard");

        easyRecords.addActionListener(e -> recordListener.showRecords(GameLevel.EASY));
        middleRecords.addActionListener(e -> recordListener.showRecords(GameLevel.MIDDLE));
        hardRecords.addActionListener(e -> recordListener.showRecords(GameLevel.HARD));

        records.add(easyRecords);
        records.add(middleRecords);
        records.add(hardRecords);

        menuBar.add(settings);
        menuBar.add(records);
    }

    public JMenuBar getMenuBar() {
        return menuBar;
    }
}
