package ru.cft.focusStar.leonova.model.events;

import ru.cft.focusStar.leonova.PositionCellOuterState;

import java.util.List;

public class OuterStateChangeEvent implements EventAcceptor {
    private List<PositionCellOuterState> newCellsOuterState;

    public OuterStateChangeEvent(List<PositionCellOuterState> newCellsOuterState) {
        this.newCellsOuterState = newCellsOuterState;
    }

    public List<PositionCellOuterState> getNewCellsOuterState() {
        return newCellsOuterState;
    }

    @Override
    public void accept(EventVisitor v) {
        v.visit(this);
    }
}
