package ru.cft.focusStar.leonova.model.events;

import ru.cft.focusStar.leonova.gameOptions.GameOptions;

public class FieldCreationEvent implements EventAcceptor{
    private GameOptions gameOptions;

    public FieldCreationEvent(GameOptions gameOptions) {
        this.gameOptions = gameOptions;
    }

    public GameOptions getGameOptions() {
        return gameOptions;
    }

    @Override
    public void accept(EventVisitor v) {
        v.visit(this);
    }
}
