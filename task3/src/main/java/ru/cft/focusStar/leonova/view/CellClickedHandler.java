package ru.cft.focusStar.leonova.view;

import ru.cft.focusStar.leonova.Position;

import java.awt.event.MouseEvent;

public interface CellClickedHandler {
    void cellClicked(MouseEvent event, Position position);
    void cellPressed(MouseEvent event);
    void cellReleased(MouseEvent event);
}
