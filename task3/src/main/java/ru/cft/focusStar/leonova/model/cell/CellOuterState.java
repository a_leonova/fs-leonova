package ru.cft.focusStar.leonova.model.cell;

public enum CellOuterState {
    CLOSED,
    OPEN,
    FLAGGED,
    QUESTION_MARK
}
