package ru.cft.focusStar.leonova.model.events;

public class WinEvent implements EventAcceptor {
    @Override
    public void accept(EventVisitor v) {
        v.visit(this);
    }
}
