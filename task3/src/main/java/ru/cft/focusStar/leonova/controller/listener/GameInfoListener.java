package ru.cft.focusStar.leonova.controller.listener;

import ru.cft.focusStar.leonova.gameOptions.GameOptions;

public interface GameInfoListener {
    void createNewGame(GameOptions gameOptions);
    void endGame();
}
