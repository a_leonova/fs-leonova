package ru.cft.focusStar.leonova.model.cell;

public enum CellInnerState {
    EMPTY,
    ONE,
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    BOMB,
    RED_BOMB,
}
