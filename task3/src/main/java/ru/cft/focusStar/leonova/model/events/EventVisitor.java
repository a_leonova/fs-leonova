package ru.cft.focusStar.leonova.model.events;

public interface EventVisitor {
    void visit(InnerStateChangeEvent event);
    void visit(LoseEvent event);
    void visit(FieldCreationEvent event);
    void visit(FlaggedPlacesCountChangeEvent event);
    void visit(TimeCountChangeEvent event);
    void visit(OuterStateChangeEvent event);
    void visit(WinEvent event);
    void visit(NewRecordEvent event);
    void visit(RecordsShowEvent event);
}
