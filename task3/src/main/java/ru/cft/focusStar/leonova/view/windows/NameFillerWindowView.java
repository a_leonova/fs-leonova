package ru.cft.focusStar.leonova.view.windows;

import ru.cft.focusStar.leonova.controller.listener.RecordListener;
import ru.cft.focusStar.leonova.model.events.NewRecordEvent;
import ru.cft.focusStar.leonova.model.record.Record;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

public class NameFillerWindowView {
    private JFrame recordFrame = new JFrame("New record");
    private RecordListener recordListener;
    private NewRecordEvent newRecordEvent;

    public NameFillerWindowView(RecordListener recordListener, NewRecordEvent newRecordEvent) {
        this.recordListener = recordListener;
        this.newRecordEvent = newRecordEvent;
        createWindow();
    }

    public void show(){
        recordFrame.setVisible(true);
    }

    private void createWindow(){
        BorderLayout layout = new BorderLayout();
        recordFrame.setLayout(layout);

        JLabel congratulation = new JLabel("NEW RECORD!");
        congratulation.setFont(congratulation.getFont().deriveFont(20.0f));
        JLabel enterName = new JLabel("Enter your name: ");
        JTextField name = new JTextField("your name");
        JButton button = new JButton("Save");

        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                recordListener.fillName(name.getText(), newRecordEvent.getRecord());
                recordFrame.dispose();
            }
        });

        recordFrame.add(congratulation, BorderLayout.NORTH);
        recordFrame.add(enterName, BorderLayout.CENTER);
        recordFrame.add(name, BorderLayout.CENTER);
        recordFrame.add(button, BorderLayout.SOUTH);
        recordFrame.pack();
    }




}
