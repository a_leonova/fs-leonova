package ru.cft.focusStar.leonova.model;

public class NeighboursIndexes {
    private int xBegin;
    private int xEnd;
    private int yBegin;
    private int yEnd;


    public NeighboursIndexes(int xBegin, int xEnd, int yBegin, int yEnd) {
        this.xBegin = xBegin;
        this.xEnd = xEnd;
        this.yBegin = yBegin;
        this.yEnd = yEnd;
    }

    public int getxBegin() {
        return xBegin;
    }

    public int getxEnd() {
        return xEnd;
    }

    public int getyBegin() {
        return yBegin;
    }

    public int getyEnd() {
        return yEnd;
    }
}
