package ru.cft.focusStar.leonova.model.cell;

import java.util.HashMap;

public class IntegerToCellInnerStateAdapter {
    private static HashMap<Integer, CellInnerState> cellStates;
    static {
        cellStates = new HashMap<>();
        cellStates.put(0, CellInnerState.EMPTY);
        cellStates.put(1, CellInnerState.ONE);
        cellStates.put(2, CellInnerState.TWO);
        cellStates.put(3, CellInnerState.THREE);
        cellStates.put(4, CellInnerState.FOUR);
        cellStates.put(5, CellInnerState.FIVE);
        cellStates.put(6, CellInnerState.SIX);
        cellStates.put(7, CellInnerState.SEVEN);
        cellStates.put(8, CellInnerState.EIGHT);
    }
    public static CellInnerState getCellState(int value){
        return cellStates.get(value);
    }
}
