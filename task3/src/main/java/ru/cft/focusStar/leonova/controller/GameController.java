package ru.cft.focusStar.leonova.controller;

import ru.cft.focusStar.leonova.gameOptions.GameOptions;
import ru.cft.focusStar.leonova.Position;
import ru.cft.focusStar.leonova.controller.listener.LeftMouseButtonClickedListener;
import ru.cft.focusStar.leonova.controller.listener.MiddleMouseButtonClickedListener;
import ru.cft.focusStar.leonova.controller.listener.GameInfoListener;
import ru.cft.focusStar.leonova.controller.listener.RightMouseButtonClickedListener;
import ru.cft.focusStar.leonova.model.FieldModel;
import ru.cft.focusStar.leonova.model.TimeProvider;

public class GameController implements LeftMouseButtonClickedListener, MiddleMouseButtonClickedListener,
        RightMouseButtonClickedListener, GameInfoListener {

    private final static int TIME_TO_SLEEP = 500;

    private FieldModel fieldModel;
    private TimeProvider timeProvider;

    private volatile boolean gameIsOn = false;

    public GameController(FieldModel fieldModel, TimeProvider timeProvider) {
        this.fieldModel = fieldModel;
        this.timeProvider = timeProvider;
    }

    @Override
    public void leftMouseButtonClicked(Position position) {
        if(!gameIsOn){
            gameIsOn = true;
            startTimer();
        }
        fieldModel.openCell(position);
    }

    @Override
    public void middleMouseButtonClicked(Position position) {
        fieldModel.openCellNeighbours(position);
    }

    @Override
    public void createNewGame(GameOptions gameOptions) {
        gameIsOn = false;
        fieldModel.createNewGame(gameOptions);
    }

    @Override
    public void endGame() {
        gameIsOn = false;
    }

    @Override
    public void rightMouseButtonClicked(Position position) {
        fieldModel.outerStateChanged(position);
    }

    private void startTimer(){
        Thread timer = new Thread(){
            @Override
            public void run() {
                super.run();
                while (gameIsOn){
                    try {
                        timeProvider.incrementTime();
                        sleep(TIME_TO_SLEEP);
                    } catch (InterruptedException e) {
                        System.err.println(e.getMessage());
                    }
                }
            }
        };
        timer.start();
    }

}
