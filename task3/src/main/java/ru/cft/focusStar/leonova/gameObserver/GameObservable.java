package ru.cft.focusStar.leonova.gameObserver;


public interface GameObservable {
    void addObserver(GameObserver obs);
    void deleteObserver(GameObserver obs);
}
