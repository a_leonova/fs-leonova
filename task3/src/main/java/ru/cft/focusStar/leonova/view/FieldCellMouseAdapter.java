package ru.cft.focusStar.leonova.view;

import ru.cft.focusStar.leonova.Position;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class FieldCellMouseAdapter extends MouseAdapter {

    private Position position;
    private CellClickedHandler handler;

    public FieldCellMouseAdapter(Position position, CellClickedHandler handler) {
        this.position = position;
        this.handler = handler;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        handler.cellClicked(e, position);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        super.mousePressed(e);
        handler.cellPressed(e);
    }
    @Override
    public void mouseReleased(MouseEvent e){
        super.mousePressed(e);
        handler.cellReleased(e);
    }

}
