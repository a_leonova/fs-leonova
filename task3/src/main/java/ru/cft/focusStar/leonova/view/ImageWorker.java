package ru.cft.focusStar.leonova.view;

import ru.cft.focusStar.leonova.model.cell.CellInnerState;
import ru.cft.focusStar.leonova.model.cell.CellOuterState;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;

public class ImageWorker {

    public void fillInnerStateImagesTable(HashMap<CellInnerState, BufferedImage> cellInnerStateImage) throws IOException {
        cellInnerStateImage.put(CellInnerState.EMPTY, ImageIO.read(ClassLoader.getSystemClassLoader().getResource("images/empty.jpg")));
        cellInnerStateImage.put(CellInnerState.ONE, ImageIO.read(ClassLoader.getSystemClassLoader().getResource("images/one.jpg")));
        cellInnerStateImage.put(CellInnerState.TWO, ImageIO.read(ClassLoader.getSystemClassLoader().getResource("images/two.jpg")));
        cellInnerStateImage.put(CellInnerState.THREE, ImageIO.read(ClassLoader.getSystemClassLoader().getResource("images/three.jpg")));
        cellInnerStateImage.put(CellInnerState.FOUR, ImageIO.read(ClassLoader.getSystemClassLoader().getResource("images/four.jpg")));
        cellInnerStateImage.put(CellInnerState.FIVE, ImageIO.read(ClassLoader.getSystemClassLoader().getResource("images/five.jpg")));
        cellInnerStateImage.put(CellInnerState.SIX, ImageIO.read(ClassLoader.getSystemClassLoader().getResource("images/six.jpg")));
        cellInnerStateImage.put(CellInnerState.SEVEN, ImageIO.read(ClassLoader.getSystemClassLoader().getResource("images/seven.jpg")));
        cellInnerStateImage.put(CellInnerState.EIGHT, ImageIO.read(ClassLoader.getSystemClassLoader().getResource("images/eight.jpg")));
        cellInnerStateImage.put(CellInnerState.BOMB, ImageIO.read(ClassLoader.getSystemClassLoader().getResource("images/openedBombNotRed.jpg")));
        cellInnerStateImage.put(CellInnerState.RED_BOMB, ImageIO.read(ClassLoader.getSystemClassLoader().getResource("images/openedBomb.jpg")));
    }

    public void fillOuterStateImagesTable(HashMap<CellOuterState, BufferedImage> cellOuterStateImage) throws IOException {
        cellOuterStateImage.put(CellOuterState.CLOSED, ImageIO.read(ClassLoader.getSystemClassLoader().getResource("images/closedCell.jpg")));
        cellOuterStateImage.put(CellOuterState.FLAGGED, ImageIO.read(ClassLoader.getSystemClassLoader().getResource("images/flag.jpg")));
        cellOuterStateImage.put(CellOuterState.QUESTION_MARK, ImageIO.read(ClassLoader.getSystemClassLoader().getResource("images/question.jpg")));
    }

    public void fillMineSweeperStateImagesTable(HashMap<MinesweeperState, BufferedImage> minesweeperStateImages) throws IOException {
        minesweeperStateImages.put(MinesweeperState.OK, ImageIO.read(ClassLoader.getSystemClassLoader().getResource("images/smileEmoji.jpg")));
        minesweeperStateImages.put(MinesweeperState.FEAR, ImageIO.read(ClassLoader.getSystemClassLoader().getResource("images/fearEmoji.jpg")));
        minesweeperStateImages.put(MinesweeperState.DEAD, ImageIO.read(ClassLoader.getSystemClassLoader().getResource("images/deadEmoji.jpg")));
        minesweeperStateImages.put(MinesweeperState.SUCCESS,ImageIO.read(ClassLoader.getSystemClassLoader().getResource("images/succEmoji.jpg")));
    }
}
