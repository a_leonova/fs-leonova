package ru.cft.focusStar.leonova.model.record;

import ru.cft.focusStar.leonova.gameOptions.GameLevel;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class RecordsHolder {
    private HashMap<GameLevel, List<Record>> gameRecords = new HashMap<>();

    private File recordFile;

    public RecordsHolder() throws IOException {
        recordFile = new File ("records.txt");
        if(!recordFile.exists()){
            recordFile.createNewFile();
        }
        parseFile(recordFile);
    }

    public boolean checkRecordAndSave(Record record) {
        if(record.getLevel() != GameLevel.SPECIAL){
            List<Record> records = gameRecords.get(record.getLevel());

            records.add(record);
            Collections.sort(records);

            while (records.size() > 10) {
                records.remove(records.size() - 1);
            }
            return records.contains(record);
        }
        return false;
    }

    public void saveRecords() throws IOException {
        recordFile.delete();
        recordFile.createNewFile();
        BufferedWriter writer = new BufferedWriter(new FileWriter(recordFile));

        List<Record> records = gameRecords.get(GameLevel.EASY);
        writeRecords(writer, records);

        records = gameRecords.get(GameLevel.MIDDLE);
        writeRecords(writer, records);

        records = gameRecords.get(GameLevel.HARD);
        writeRecords(writer, records);

        writer.flush();
    }

    public List<Record> getLevelRecordList(GameLevel level){
        return gameRecords.get(level);
    }

    private void writeRecords(BufferedWriter writer, List<Record> records) throws IOException {
        for(Record record : records){
            writer.write(record.getLevel() + Record.SPLITTER + record.getName() + Record.SPLITTER + record.getTime() + "\n");
        }
    }

    private void parseFile(File recordFile) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(recordFile));
        ArrayList<Record> easyRecords = new ArrayList<>();
        ArrayList<Record> middleRecords = new ArrayList<>();
        ArrayList<Record> hardRecords = new ArrayList<>();

        String line;
        while((line = reader.readLine()) != null){
            try {
                Record record = new Record(line);
                switch (record.getLevel()){
                    case EASY:
                        easyRecords.add(record);
                        break;
                    case MIDDLE:
                        middleRecords.add(record);
                        break;
                    case HARD:
                        hardRecords.add(record);
                }
            }catch (IllegalArgumentException ignore){}
        }
        gameRecords.put(GameLevel.EASY, easyRecords);
        gameRecords.put(GameLevel.MIDDLE, middleRecords);
        gameRecords.put(GameLevel.HARD, hardRecords);
    }
}
