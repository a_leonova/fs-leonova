package ru.cft.focusStar.leonova.model;

import ru.cft.focusStar.leonova.PositionCellInnerState;
import ru.cft.focusStar.leonova.PositionCellOuterState;

import java.util.ArrayList;
import java.util.List;

public class OpeningResult {
    private GameState gameState = GameState.PLAY;
    private List<PositionCellInnerState> openedCells = new ArrayList<>();
    private List<PositionCellOuterState> changedOuterStateCells = new ArrayList<>();

    public GameState getGameState() {
        return gameState;
    }

    public List<PositionCellInnerState> getOpenedCells() {
        return openedCells;
    }

    public List<PositionCellOuterState> getChangedOuterStateCells() {
        return changedOuterStateCells;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public void setOpenedCells(List<PositionCellInnerState> openedCells) {
        this.openedCells = openedCells;
    }

    public void setChangedOuterStateCells(List<PositionCellOuterState> changedOuterStateCells) {
        this.changedOuterStateCells = changedOuterStateCells;
    }
}
