package ru.cft.focusStar.leonova.model.events;

import ru.cft.focusStar.leonova.PositionCellInnerState;
import ru.cft.focusStar.leonova.Position;
import ru.cft.focusStar.leonova.model.cell.CellInnerState;

import java.util.List;

public class InnerStateChangeEvent implements EventAcceptor {

    private List<PositionCellInnerState> newCellsInnerState;

    public InnerStateChangeEvent(List<PositionCellInnerState> newCellsInnerState) {
        this.newCellsInnerState = newCellsInnerState;
    }

    public List<PositionCellInnerState> getNewCellsInnerState() {
        return newCellsInnerState;
    }

    @Override
    public void accept(EventVisitor v) {
        v.visit(this);
    }
}
