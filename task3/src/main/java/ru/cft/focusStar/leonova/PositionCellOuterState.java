package ru.cft.focusStar.leonova;

import ru.cft.focusStar.leonova.model.cell.CellOuterState;

public class PositionCellOuterState {
    private Position position;
    private CellOuterState outerState;

    public PositionCellOuterState(Position position, CellOuterState outerState) {
        this.position = position;
        this.outerState = outerState;
    }

    public Position getPosition() {
        return position;
    }

    public CellOuterState getOuterState() {
        return outerState;
    }
}
