package ru.cft.focusStar.leonova.model;

import ru.cft.focusStar.leonova.gameOptions.GameLevel;
import ru.cft.focusStar.leonova.gameOptions.GameOptions;
import ru.cft.focusStar.leonova.Position;
import ru.cft.focusStar.leonova.gameObserver.GameObservable;
import ru.cft.focusStar.leonova.gameObserver.GameObserver;
import ru.cft.focusStar.leonova.model.cell.CellOuterState;
import ru.cft.focusStar.leonova.model.field.Field;
import ru.cft.focusStar.leonova.model.field.UsualField;
import ru.cft.focusStar.leonova.model.record.Record;
import ru.cft.focusStar.leonova.model.record.RecordsHolder;
import ru.cft.focusStar.leonova.model.record.RecordsProvider;

import java.io.IOException;
import java.util.*;

public class FieldModelHolder implements FieldModel, GameObservable, RecordsProvider, TimeProvider  {
    private boolean gameIsOn = false;
    private GameLevel gameLevel;

    private Field field;
    private Notifier notifier;
    private RecordsHolder recordsHolder;

    private long gameTime;
    private long startTime;

    public FieldModelHolder() throws IOException {
        notifier = new Notifier();
        field = new UsualField();
        recordsHolder = new RecordsHolder();
    }

    @Override
    public void openCell(Position position) {
        if(!gameIsOn){
            initializeField(position);
        }
        if(field.getCell(position).getOuterState() == CellOuterState.CLOSED){
            OpeningResult result = field.openCell(Collections.singletonList(position));
            checkResult(result);
            notifier.openCells(result.getOpenedCells());
            notifier.outerStateChanged(result.getChangedOuterStateCells());
        }
    }

    @Override
    public void openCellNeighbours(Position position) {
        if(field.getCell(position).getOuterState() == CellOuterState.OPEN){
            OpeningResult result = field.openCellAndNeighbours(position);
            checkResult(result);
            notifier.outerStateChanged(result.getChangedOuterStateCells());
            notifier.openCells(result.getOpenedCells());
        }
    }

    @Override
    public void outerStateChanged(Position positions) {
        if(field.getCell(positions).getOuterState() != CellOuterState.OPEN){
            field.changeOuterStateForNext(positions);
            notifier.outerStateChanged(field.createPairsPositionOuterState(Collections.singletonList(positions)));
            notifier.newCountFlaggedPlace(field.getNotFlaggedPlaces());
        }
    }

    @Override
    public void createNewGame(GameOptions gameOptions) {
        gameIsOn = false;
        gameLevel = gameOptions.getLevel();
        field.createNewField(gameOptions);
        notifier.createNewField(gameOptions);
    }

    @Override
    public void initializeField(Position position) {
        gameIsOn = true;
        startTime = System.nanoTime();
        field.initializeField(position);
    }

    @Override
    public void addObserver(GameObserver obs) {
        notifier.addObserver(obs);
    }

    @Override
    public void deleteObserver(GameObserver obs) {
        notifier.deleteObserver(obs);
    }

    private void checkResult(OpeningResult result){
        switch (result.getGameState()){
            case END_LOSE:
                gameIsOn = false;
                notifier.loseGame();
                break;
            case END_WIN:
                gameIsOn = false;
                checkNewRecord();
                notifier.win();
                break;
        }
    }

    private void checkNewRecord(){
        Record record = new Record(gameLevel, (int) (gameTime / 1000000000));
        if(recordsHolder.checkRecordAndSave(record)){
            notifier.fillNameWhenNewRecord(record);
        }
    }

    @Override
    public void saveRecords() throws IOException {
        recordsHolder.saveRecords();
    }

    @Override
    public void showRecords(GameLevel level) {
        notifier.showRecords(recordsHolder.getLevelRecordList(level));
    }

    @Override
    public void incrementTime() {
        gameTime = System.nanoTime() - startTime;
        notifier.changeTime((int)(gameTime / 1000000000));
    }
}
