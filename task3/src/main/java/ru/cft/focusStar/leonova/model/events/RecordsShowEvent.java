package ru.cft.focusStar.leonova.model.events;

import ru.cft.focusStar.leonova.model.record.Record;

import java.util.List;

public class RecordsShowEvent implements EventAcceptor {

    private List<Record> records;

    public RecordsShowEvent(List<Record> records) {
        this.records = records;
    }

    public List<Record> getRecords() {
        return records;
    }

    @Override
    public void accept(EventVisitor v) {
        v.visit(this);
    }
}
