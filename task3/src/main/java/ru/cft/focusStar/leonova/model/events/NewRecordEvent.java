package ru.cft.focusStar.leonova.model.events;

import ru.cft.focusStar.leonova.model.record.Record;

public class NewRecordEvent implements EventAcceptor {

    private Record record;

    public NewRecordEvent(Record record) {
        this.record = record;
    }

    public Record getRecord() {
        return record;
    }

    @Override
    public void accept(EventVisitor v) {
        v.visit(this);
    }
}
