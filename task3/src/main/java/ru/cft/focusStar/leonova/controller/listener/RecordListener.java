package ru.cft.focusStar.leonova.controller.listener;

import ru.cft.focusStar.leonova.gameOptions.GameLevel;
import ru.cft.focusStar.leonova.model.record.Record;


public interface RecordListener {
    void showRecords(GameLevel level);
    void fillName(String name, Record unfilledRecord);

}
