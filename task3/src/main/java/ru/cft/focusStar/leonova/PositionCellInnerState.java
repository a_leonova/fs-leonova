package ru.cft.focusStar.leonova;

import ru.cft.focusStar.leonova.model.cell.CellInnerState;

public class PositionCellInnerState{
    private Position position;
    private CellInnerState innerState;

    public PositionCellInnerState(Position position, CellInnerState innerState) {
        this.position = position;
        this.innerState = innerState;
    }

    public Position getPosition() {
        return position;
    }

    public CellInnerState getInnerState() {
        return innerState;
    }
}
