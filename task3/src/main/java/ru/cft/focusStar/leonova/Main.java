package ru.cft.focusStar.leonova;

import ru.cft.focusStar.leonova.controller.GameController;
import ru.cft.focusStar.leonova.controller.RecordController;
import ru.cft.focusStar.leonova.gameOptions.GameOptions;
import ru.cft.focusStar.leonova.model.FieldModelHolder;
import ru.cft.focusStar.leonova.view.View;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        new Main().go();
    }

    public void go() throws IOException {
        FieldModelHolder model = new FieldModelHolder();
        GameController gameController = new GameController(model, model);
        RecordController recordController = new RecordController(model);
        View view = new View();

        view.setLeftMouseButtonClickedListener(gameController);
        view.setMiddleMouseButtonClickedListener(gameController);
        view.setRightMouseButtonClickedListener(gameController);
        view.setGameInfoListener(gameController);
        view.setRecordListener(recordController);

        model.addObserver(view);

        gameController.createNewGame(GameOptions.EASY);
    }

}
