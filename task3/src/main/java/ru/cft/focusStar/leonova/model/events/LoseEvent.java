package ru.cft.focusStar.leonova.model.events;

public class LoseEvent implements EventAcceptor {
    @Override
    public void accept(EventVisitor v) {
        v.visit(this);
    }
}
