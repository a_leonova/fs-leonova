package ru.cft.focusStar.leonova.view.panels;

import ru.cft.focusStar.leonova.PositionCellInnerState;
import ru.cft.focusStar.leonova.Position;
import ru.cft.focusStar.leonova.PositionCellOuterState;
import ru.cft.focusStar.leonova.controller.listener.LeftMouseButtonClickedListener;
import ru.cft.focusStar.leonova.controller.listener.MiddleMouseButtonClickedListener;
import ru.cft.focusStar.leonova.controller.listener.RightMouseButtonClickedListener;
import ru.cft.focusStar.leonova.model.cell.CellInnerState;
import ru.cft.focusStar.leonova.model.cell.CellOuterState;
import ru.cft.focusStar.leonova.view.CellClickedHandler;
import ru.cft.focusStar.leonova.view.FieldCellMouseAdapter;
import ru.cft.focusStar.leonova.view.ImageWorker;
import ru.cft.focusStar.leonova.view.MinesweeperState;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FieldPanel implements CellClickedHandler {
    private JPanel lowerPanel = new JPanel();

    private HashMap<CellInnerState, BufferedImage> cellInnerStateImages = new HashMap<>();
    private HashMap<CellOuterState, BufferedImage> cellOuterStateImages = new HashMap<>();

    private LeftMouseButtonClickedListener leftMouseButtonClickedListener;
    private RightMouseButtonClickedListener rightMouseButtonClickedListener;
    private MiddleMouseButtonClickedListener middleMouseButtonClickedListener;

    private TopPanel topPanel;

    private ArrayList<JButton> cells;
    private int width;
    private int height;

    public FieldPanel(TopPanel topPanel) throws IOException {
        ImageWorker imageWorker = new ImageWorker();
        imageWorker.fillInnerStateImagesTable(cellInnerStateImages);
        imageWorker.fillOuterStateImagesTable(cellOuterStateImages);

        this.topPanel = topPanel;
    }

    public void setLeftMouseButtonClickedListener(LeftMouseButtonClickedListener leftMouseButtonClickedListener) {
        this.leftMouseButtonClickedListener = leftMouseButtonClickedListener;
    }

    public void setRightMouseButtonClickedListener(RightMouseButtonClickedListener rightMouseButtonClickedListener) {
        this.rightMouseButtonClickedListener = rightMouseButtonClickedListener;
    }

    public void setMiddleMouseButtonClickedListener(MiddleMouseButtonClickedListener middleMouseButtonClickedListener) {
        this.middleMouseButtonClickedListener = middleMouseButtonClickedListener;
    }

    public JPanel getLowerPanel() {
        return lowerPanel;
    }

    public void updateOuterCellState(List<PositionCellOuterState> newCellsOuterState) {
        for(PositionCellOuterState newState : newCellsOuterState){
            JButton button = cells.get(positionToInt(newState.getPosition()));
            button.setIcon(new ImageIcon(cellOuterStateImages.get(newState.getOuterState())));
        }
    }

    public void updateOpenedCells(List<PositionCellInnerState> newCellsInnerState) {
        for(PositionCellInnerState newState : newCellsInnerState){
            JButton button = cells.get(positionToInt(newState.getPosition()));
            button.setIcon(new ImageIcon(cellInnerStateImages.get(newState.getInnerState())));
        }
    }

    public void createNewField(int height, int width){
        this.height = height;
        this.width = width;

        lowerPanel.removeAll();
        GridLayout gridLayout = new GridLayout(height, width);
        lowerPanel.setLayout(gridLayout);
        lowerPanel.setSize(cellOuterStateImages.get(CellOuterState.CLOSED).getWidth() * width,
                cellOuterStateImages.get(CellOuterState.CLOSED).getHeight() * height);
        cells = new ArrayList<>(height*width);
        for(int i = 0 ; i < width * height; ++i){
            final int x = i % width;
            final int y = i / width;
            JButton button = new JButton((new ImageIcon(cellOuterStateImages.get(CellOuterState.CLOSED))));
            button.setSize(cellOuterStateImages.get(CellOuterState.CLOSED).getWidth(),
                    cellOuterStateImages.get(CellOuterState.CLOSED).getHeight());
            cells.add(i, button);
            button.addMouseListener(new FieldCellMouseAdapter(new Position(x, y), this));
            button.setBorder(BorderFactory.createEmptyBorder());
            lowerPanel.add(button, i);
        }
    }

    private int positionToInt(Position position){
        return position.getY() * width + position.getX();
    }

    @Override
    public void cellClicked(MouseEvent event, Position position) {
        if(SwingUtilities.isLeftMouseButton(event)){
            leftMouseButtonClickedListener.leftMouseButtonClicked(position);
        }
        if(SwingUtilities.isRightMouseButton(event)){
            rightMouseButtonClickedListener.rightMouseButtonClicked(position);
        }
        if(SwingUtilities.isMiddleMouseButton(event)){
            middleMouseButtonClickedListener.middleMouseButtonClicked(position);
            //openAllNeighbours
        }
    }

    @Override
    public void cellPressed(MouseEvent event) {
        if(SwingUtilities.isLeftMouseButton(event)){
            topPanel.setSmileState(MinesweeperState.FEAR);
        }
    }

    @Override
    public void cellReleased(MouseEvent event) {
        if(SwingUtilities.isLeftMouseButton(event)) {
            topPanel.setSmileState(MinesweeperState.OK);
        }
    }
}
