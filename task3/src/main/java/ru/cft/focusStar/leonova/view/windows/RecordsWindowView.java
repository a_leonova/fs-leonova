package ru.cft.focusStar.leonova.view.windows;

import ru.cft.focusStar.leonova.model.record.Record;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class RecordsWindowView {
    private List<Record> records;
    private JFrame window;

    public RecordsWindowView(List<Record> records) {
        this.records = records;
        createWindow();
    }

    public void show(){
        window.setVisible(true);
    }

    private void createWindow(){
        window = new JFrame("Records");
        GridLayout gridLayout = new GridLayout(records.size() + 1,1);
        window.setLayout(gridLayout);

        for(int i = 0; i < records.size(); ++i){
            JLabel label = new JLabel(i + 1 + ". " + records.get(i).getName() + " : " + records.get(i).getTime());
            label.setFont(label.getFont().deriveFont(20.0f));
            window.add(label, i);
        }
        window.pack();
    }
}
