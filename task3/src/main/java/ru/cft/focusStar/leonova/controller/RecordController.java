package ru.cft.focusStar.leonova.controller;

import ru.cft.focusStar.leonova.controller.listener.RecordListener;
import ru.cft.focusStar.leonova.gameOptions.GameLevel;
import ru.cft.focusStar.leonova.model.record.Record;
import ru.cft.focusStar.leonova.model.record.RecordsProvider;

import java.io.IOException;

public class RecordController implements RecordListener {
    private RecordsProvider recordsProvider;

    public RecordController(RecordsProvider recordsProvider) {
        this.recordsProvider = recordsProvider;
    }


    @Override
    public void showRecords(GameLevel level) {
        recordsProvider.showRecords(level);
    }

    @Override
    public void fillName(String name, Record unfilledRecord) {
        unfilledRecord.setName(name);
        saveRecords();
    }

    private void saveRecords() {
        try {
            recordsProvider.saveRecords();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
