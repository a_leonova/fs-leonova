package ru.cft.focusStar.leonova.model.events;

public interface EventAcceptor {
    void accept(EventVisitor v);
}
