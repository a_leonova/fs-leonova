package ru.cft.focusStar.leonova.gameOptions;

public class GameOptions {

    public static GameOptions EASY = new GameOptions(GameLevel.EASY,9,9, 10);
    public static GameOptions MIDDLE = new GameOptions(GameLevel.MIDDLE,16,16, 40);
    public static GameOptions HARD = new GameOptions(GameLevel.HARD,30,16, 99);


    public static final int FROM = 1;
    public static final int TO = 100;

    private GameLevel level;
    private int width;
    private int height;
    private int mineCount;

    public GameOptions(GameLevel level, int width, int height, int mineCount) {

        if(width < FROM || height < FROM || mineCount < FROM){
            throw new IllegalArgumentException("Invalid value: must be positive");
        }
        if ( width > TO || height > TO){
            throw new IllegalArgumentException("Invalid field size: must be <= " + TO);
        }
        if(width * height <= mineCount){
            throw new IllegalArgumentException("Bomb's number must be less then field's cells");
        }

        this.level = level;
        this.width = width;
        this.height = height;
        this.mineCount = mineCount;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getMineCount() {
        return mineCount;
    }

    public GameLevel getLevel() {
        return level;
    }
}
