package ru.cft.focusStar.leonova.model;

public interface TimeProvider {
    void incrementTime();
}
