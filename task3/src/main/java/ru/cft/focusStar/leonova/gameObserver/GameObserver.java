package ru.cft.focusStar.leonova.gameObserver;

import ru.cft.focusStar.leonova.model.events.EventAcceptor;

public interface GameObserver {
    void update(EventAcceptor event);
}
