package ru.cft.focusStar.leonova.model.cell;

public class Cell {

    private boolean bomb;

    private int countNeighboursWithBombs;
    private CellInnerState innerState;
    private CellOuterState outerState = CellOuterState.CLOSED;

    public void setBomb(boolean bomb) {
        this.bomb = bomb;
        if(bomb){
            innerState = CellInnerState.BOMB;
        }
    }

    public void setOuterState(CellOuterState outerState) {
        this.outerState = outerState;
    }

    public void setCountNeighboursWithBomb(int countNeighboursWithBomb) {
        if(countNeighboursWithBomb < 0 || countNeighboursWithBomb > 8){
            throw new IllegalArgumentException("Can't have this number of mines");
        }
        if(!bomb){
            this.countNeighboursWithBombs = countNeighboursWithBomb;
            CellInnerState cellState = IntegerToCellInnerStateAdapter.getCellState(countNeighboursWithBomb);
            innerState = cellState;
        }
    }

    public int getNeighboursWithBomb(){
        return countNeighboursWithBombs;
    }

    public CellInnerState getInnerState() {
        return innerState;
    }

    public CellOuterState getOuterState() {
        return outerState;
    }

    public void openCell(){
        outerState = CellOuterState.OPEN;
    }

    public void explodedBomb(){
        innerState = CellInnerState.RED_BOMB;
    }

}
