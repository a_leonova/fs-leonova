package ru.cft.focusStar.leonova.view.windows;

import ru.cft.focusStar.leonova.gameOptions.GameLevel;
import ru.cft.focusStar.leonova.gameOptions.GameOptions;
import ru.cft.focusStar.leonova.controller.listener.GameInfoListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ManualSettingWindow {
    private JFrame settingsFrame = new JFrame("Settings");
    private JTextField widthTF;
    private JTextField heightTF;
    private JTextField minesTF;

    private GameInfoListener gameInfoListener;

    public ManualSettingWindow(GameInfoListener gameInfoListener) {
        this.gameInfoListener = gameInfoListener;
        createWindow();
    }

    public void show(){
        settingsFrame.setVisible(true);
    }

    private void createWindow(){
        GridLayout gridLayout = new GridLayout(4,2);
        settingsFrame.setLayout(gridLayout);

        JLabel width = new JLabel("Width (" + GameOptions.FROM + "- " + GameOptions.TO + "): ");
        JLabel height = new JLabel("Height (" + GameOptions.FROM + "- " + GameOptions.TO + "): ");
        JLabel minesCount = new JLabel("Count of mines:");

        widthTF = new JTextField("1");
        heightTF = new JTextField("1");
        minesTF = new JTextField("1");

        JButton button = new JButton("Create");
        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if(checkParams()){
                    settingsFrame.dispose();
                }
            }
        });

        settingsFrame.add(width);
        settingsFrame.add(widthTF);
        settingsFrame.add(height);
        settingsFrame.add(heightTF);
        settingsFrame.add(minesCount);
        settingsFrame.add(minesTF);
        settingsFrame.add(button);

        settingsFrame.pack();
    }

    private boolean checkParams(){
        int height;
        int width;
        int bombsNumber;

        try{
            height = Integer.parseInt(heightTF.getText());
            width = Integer.parseInt(widthTF.getText());
            bombsNumber = Integer.parseInt(minesTF.getText());
            gameInfoListener.createNewGame(new GameOptions(GameLevel.SPECIAL ,width, height, bombsNumber));
        }catch (IllegalArgumentException e){
            showMessage(e.getMessage());
            return false;
        }
        return true;
    }

    private void showMessage(String message){
        JFrame frame = new JFrame("Error");
        FlowLayout layout = new FlowLayout();
        frame.setLayout(layout);
        JLabel label = new JLabel(message);
        frame.add(label);
        frame.pack();
        frame.setVisible(true);
    }
}
