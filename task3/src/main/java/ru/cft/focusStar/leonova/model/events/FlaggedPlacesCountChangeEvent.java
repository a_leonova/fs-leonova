package ru.cft.focusStar.leonova.model.events;

public class FlaggedPlacesCountChangeEvent implements EventAcceptor{

    private int flaggedPlaces;

    public FlaggedPlacesCountChangeEvent(int minesNumber) {
        this.flaggedPlaces = minesNumber;
    }

    public int getFlaggedPlaces() {
        return flaggedPlaces;
    }

    @Override
    public void accept(EventVisitor v) {
        v.visit(this);
    }
}
