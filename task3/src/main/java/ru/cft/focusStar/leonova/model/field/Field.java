package ru.cft.focusStar.leonova.model.field;

import ru.cft.focusStar.leonova.PositionCellOuterState;
import ru.cft.focusStar.leonova.gameOptions.GameOptions;
import ru.cft.focusStar.leonova.Position;
import ru.cft.focusStar.leonova.model.OpeningResult;
import ru.cft.focusStar.leonova.model.cell.Cell;

import java.util.List;

public interface Field {
    void createNewField(GameOptions gameOptions);
    OpeningResult openCell(List<Position> positions);
    void changeOuterStateForNext(Position position);
    List<PositionCellOuterState> createPairsPositionOuterState(List<Position> positions);
    OpeningResult openCellAndNeighbours(Position position);
    void initializeField(Position startPosition);
    int getNotFlaggedPlaces();

    Cell getCell(Position position);

}
