package ru.cft.focusStar.leonova.gameOptions;

public enum GameLevel {
    EASY,
    MIDDLE,
    HARD,
    SPECIAL
}
