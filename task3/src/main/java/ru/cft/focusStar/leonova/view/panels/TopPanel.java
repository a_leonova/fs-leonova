package ru.cft.focusStar.leonova.view.panels;

import ru.cft.focusStar.leonova.gameOptions.GameOptions;
import ru.cft.focusStar.leonova.controller.listener.GameInfoListener;
import ru.cft.focusStar.leonova.view.ImageWorker;
import ru.cft.focusStar.leonova.view.MinesweeperState;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;

public class TopPanel {

    private GameOptions lastGameOption;
    private GameInfoListener gameInfoListener;

    private JLabel minesCount;
    private JLabel timer;
    private JLabel smileLabel;
    private HashMap<MinesweeperState, BufferedImage> minesweeperStateImages = new HashMap<>();

    private JPanel upperPanel = new JPanel();

    public TopPanel() throws IOException {
        ImageWorker imageWorker = new ImageWorker();
        imageWorker.fillMineSweeperStateImagesTable(minesweeperStateImages);
        fillPanel();
    }

    public void setGameInfoListener(GameInfoListener gameInfoListener) {
        this.gameInfoListener = gameInfoListener;
    }

    public void gameOver(){
        gameInfoListener.endGame();
        smileLabel.setIcon(new ImageIcon(minesweeperStateImages.get(MinesweeperState.DEAD)));
    }

    public void win(){
        gameInfoListener.endGame();
        smileLabel.setIcon(new ImageIcon(minesweeperStateImages.get(MinesweeperState.SUCCESS)));
    }

    public void newMinesCount(int minesCount){
        this.minesCount.setText(String.valueOf(minesCount));
    }

    public void changeTime(int changeTime) {
        timer.setText(String.valueOf(changeTime));
    }

    public JPanel getUpperPanel() {
        return upperPanel;
    }

    public void setSmileState(MinesweeperState state){
        smileLabel.setIcon(new ImageIcon(minesweeperStateImages.get(state)));
    }

    public void newGamePreparation(GameOptions gameOptions){
        setSmileState(MinesweeperState.OK);
        minesCount.setText(String.valueOf(gameOptions.getMineCount()));
        timer.setText("0");
        lastGameOption = gameOptions;
    }

    private void fillPanel(){
        FlowLayout layout = new FlowLayout();
        upperPanel.setLayout(layout);

        minesCount = new JLabel();
        smileLabel = new JLabel((new ImageIcon(minesweeperStateImages.get(MinesweeperState.OK))));
        timer = new JLabel("0");

        minesCount.setFont(minesCount.getFont().deriveFont(64.0f));
        timer.setFont(timer.getFont().deriveFont(64.0f));

        smileLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                gameInfoListener.createNewGame(lastGameOption);
                timer.setText("0");
            }
        });

        upperPanel.add(minesCount);
        upperPanel.add(smileLabel);
        upperPanel.add(timer);
    }
}
