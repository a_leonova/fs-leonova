package ru.cft.focusStar.leonova.view;

import ru.cft.focusStar.leonova.controller.RecordController;
import ru.cft.focusStar.leonova.controller.listener.LeftMouseButtonClickedListener;
import ru.cft.focusStar.leonova.controller.listener.MiddleMouseButtonClickedListener;
import ru.cft.focusStar.leonova.controller.listener.GameInfoListener;
import ru.cft.focusStar.leonova.controller.listener.RightMouseButtonClickedListener;
import ru.cft.focusStar.leonova.gameObserver.GameObserver;
import ru.cft.focusStar.leonova.model.events.*;
import ru.cft.focusStar.leonova.view.menus.GameMenu;
import ru.cft.focusStar.leonova.view.panels.FieldPanel;
import ru.cft.focusStar.leonova.view.panels.TopPanel;
import ru.cft.focusStar.leonova.view.windows.NameFillerWindowView;
import ru.cft.focusStar.leonova.view.windows.RecordsWindowView;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class View implements GameObserver, EventVisitor {
    private RecordController recordListener;

    private JFrame frame;

    private TopPanel topPanel;

    private FieldPanel fieldPanel;
    private GameMenu menu;
    public View() throws IOException {

        frame = new JFrame("Сапёр");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        topPanel = new TopPanel();
        fieldPanel = new FieldPanel(topPanel);
        menu = new GameMenu();

        frame.add(topPanel.getUpperPanel(), BorderLayout.NORTH);
        frame.add(fieldPanel.getLowerPanel(), BorderLayout.CENTER);
        frame.setJMenuBar(menu.getMenuBar());

        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public void setLeftMouseButtonClickedListener(LeftMouseButtonClickedListener leftMouseButtonClickedListener) {
        fieldPanel.setLeftMouseButtonClickedListener(leftMouseButtonClickedListener);
    }

    public void setRightMouseButtonClickedListener(RightMouseButtonClickedListener rightMouseButtonClickedListener) {
        fieldPanel.setRightMouseButtonClickedListener(rightMouseButtonClickedListener);
    }

    public void setMiddleMouseButtonClickedListener(MiddleMouseButtonClickedListener middleMouseButtonClickedListener) {
        fieldPanel.setMiddleMouseButtonClickedListener(middleMouseButtonClickedListener);
    }

    public void setGameInfoListener(GameInfoListener gameInfoListener) {
        topPanel.setGameInfoListener(gameInfoListener);
        menu.setGameInfoListener(gameInfoListener);
    }

    public void setRecordListener(RecordController recordListener) {
        this.recordListener = recordListener;
        menu.setRecordListener(recordListener);
    }

    @Override
    public void update(EventAcceptor event) {
        event.accept(this);
    }

    @Override
    public void visit(InnerStateChangeEvent event) {
        fieldPanel.updateOpenedCells(event.getNewCellsInnerState());
    }

    @Override
    public void visit(LoseEvent event) {
        topPanel.gameOver();
    }

    @Override
    public void visit(FieldCreationEvent event) {
        topPanel.newGamePreparation(event.getGameOptions());
        fieldPanel.createNewField(event.getGameOptions().getHeight(),event.getGameOptions().getWidth());
        frame.pack();
    }

    @Override
    public void visit(FlaggedPlacesCountChangeEvent event) {
        topPanel.newMinesCount(event.getFlaggedPlaces());
    }

    @Override
    public void visit(TimeCountChangeEvent event) {
        topPanel.changeTime(event.getTime());
    }

    @Override
    public void visit(OuterStateChangeEvent event) {
        fieldPanel.updateOuterCellState(event.getNewCellsOuterState());
    }

    @Override
    public void visit(WinEvent event) {
        topPanel.win();
    }

    @Override
    public void visit(NewRecordEvent event){
        NameFillerWindowView fillNameRecordWindow = new NameFillerWindowView(recordListener, event);
        fillNameRecordWindow.show();
    }

    @Override
    public void visit(RecordsShowEvent event) {
        RecordsWindowView recordsWindow = new RecordsWindowView(event.getRecords());
        recordsWindow.show();
    }


}
