package ru.cft.focusStar.leonova.controller.listener;

import ru.cft.focusStar.leonova.Position;

public interface LeftMouseButtonClickedListener {
    void leftMouseButtonClicked(Position position);
}
