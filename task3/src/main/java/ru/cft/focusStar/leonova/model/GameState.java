package ru.cft.focusStar.leonova.model;

public enum GameState {
    END_LOSE,
    END_WIN,
    PLAY
}
