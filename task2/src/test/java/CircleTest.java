import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.cft.focusStar.leonova.Shapes.AdditionalInfoHolder;
import ru.cft.focusStar.leonova.Shapes.Circle;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.closeTo;


public class CircleTest {
    //giving bad args to constructor was checked into FactoryTest

    @Test
    public void circleCheckArea(){
        Circle circle = new Circle(1.0);
        assertThat(circle.getArea(), is(closeTo(3.1415, 0.01)));
    }

    @Test
    public void circleCheckPerimeter(){
        Circle circle = new Circle(5.0);
        assertThat(circle.getPerimeter(), is(closeTo(31.415, 0.01)));
    }
    @Test
    public void circleCheckRadius() {
        Circle circle = new Circle(5.0);
        for(AdditionalInfoHolder info : circle.getAdditionalCharacteristics()){
            if(info.getCharacteristicName().equals("Радиус")){
                assertThat(info.getCharacteristicValue(), is(closeTo(5.0, 0.001)));
                return;
            }
        }
        Assertions.fail("Not found characteristicInfo with \"Радиус\" name");
    }
    @Test
    public void circleCheckDiameter() {
        Circle circle = new Circle(5.0);
        for(AdditionalInfoHolder info : circle.getAdditionalCharacteristics()){
            if(info.getCharacteristicName().equals("Диаметр")){
                assertThat(info.getCharacteristicValue(), is(closeTo(10.0, 0.001)));
                return;
            }
        }
        Assertions.fail("Not found characteristicInfo with \"Диаметр\" name");
    }

}
