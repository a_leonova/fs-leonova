import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.cft.focusStar.leonova.Shapes.AdditionalInfoHolder;
import ru.cft.focusStar.leonova.Shapes.Rectangle;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.closeTo;

public class RectangleTest {

    @Test
    public void rectangleCheckArea(){
        Rectangle rectangle = new Rectangle(5.0, 4.0);
        assertThat(rectangle.getArea(), is(closeTo(20.0, 0.01)));
    }

    @Test
    public void rectangleCheckPerimeter(){
        Rectangle rectangle = new Rectangle(5.0, 4.0);
        assertThat(rectangle.getPerimeter(), is(closeTo(18.0, 0.01)));
    }

    @Test
    public void rectangleCheckDiagonal() {
        Rectangle rectangle = new Rectangle(4.0, 3.0);
        for(AdditionalInfoHolder info : rectangle.getAdditionalCharacteristics()){
            if(info.getCharacteristicName().equals("Диагональ")){
                assertThat(info.getCharacteristicValue(), is(closeTo(5.0, 0.01)));
                return;
            }
        }
        Assertions.fail("Not found characteristicInfo with \"Диагональ\" name");
    }

    @Test
    public void rectangleCheckWidth() {
        Rectangle rectangle = new Rectangle(4.0, 3.0);
        for(AdditionalInfoHolder info : rectangle.getAdditionalCharacteristics()){
            if(info.getCharacteristicName().equals("Ширина")){
                assertThat(info.getCharacteristicValue(), is(closeTo(3.0, 0.01)));
                return;
            }
        }
        Assertions.fail("Not found characteristicInfo with \"Ширина\" name");
    }

    @Test
    public void rectangleCheckHeight() {
        Rectangle rectangle = new Rectangle(4.0, 3.0);
        for(AdditionalInfoHolder info : rectangle.getAdditionalCharacteristics()){
            if(info.getCharacteristicName().equals("Длина")){
                assertThat(info.getCharacteristicValue(), is(closeTo(4.0, 0.01)));
                return;
            }
        }
        Assertions.fail("Not found characteristicInfo with \"Длина\" name");
    }



}
