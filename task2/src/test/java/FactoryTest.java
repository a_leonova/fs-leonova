import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.cft.focusStar.leonova.ShapeFactory.Factory;
import ru.cft.focusStar.leonova.ShapeFactory.ShapeProvider;
import ru.cft.focusStar.leonova.Shapes.Circle;
import ru.cft.focusStar.leonova.Shapes.Rectangle;
import ru.cft.focusStar.leonova.Shapes.Shape;
import ru.cft.focusStar.leonova.Shapes.Triangle;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FactoryTest {

    private Factory factory;
    private ShapeProvider shapeProvider;

    @BeforeEach
    public void initialize(){
        factory = new Factory();
        shapeProvider = new ShapeProvider();
        shapeProvider.addShapesToFactory(factory);
    }

    @Test
    public void factoryCreate_returnCircle_ifCircleArg(){
        Shape circle = factory.createShape("CIRCLE", "5");
        assertEquals(circle.getClass().getCanonicalName(), Circle.class.getCanonicalName());
    }
    @Test
    public void factoryCreate_returnRectangle_ifRectangleArg() {
        Shape rectangle = factory.createShape("RECTANGLE", "5 2");
        assertEquals(rectangle.getClass().getCanonicalName(), Rectangle.class.getCanonicalName());
    }

    @Test
    public void factoryCreate_returnTriangle_ifTriangleArg()  {
        Shape triangle = factory.createShape("TRIANGLE", "3 4 5");
        assertEquals(triangle.getClass().getCanonicalName(), Triangle.class.getCanonicalName());
    }

    @Test
    public void factoryCreate_throwIllegalArgumentException_ifBadArgsForCircle(){
        Throwable exception = assertThrows(IllegalArgumentException.class, ()->factory.createShape("CIRCLE", "5 10 6"));
        assertEquals("Bad number of characteristic: need 1", exception.getMessage());
    }

    @Test
    public void factoryCreate_throwIllegalArgumentException_ifBadArgsForRectangle(){
        Throwable exception = assertThrows(IllegalArgumentException.class, ()->factory.createShape("RECTANGLE", "5"));
        assertEquals("Bad number of characteristic: need 2", exception.getMessage());
    }

    @Test
    public void factoryCreate_throwIllegalArgumentException_ifBadArgsForTriangle(){
        Throwable exception = assertThrows(IllegalArgumentException.class, ()->factory.createShape("TRIANGLE", "5 10"));
        assertEquals("Bad number of characteristic: need 3", exception.getMessage());
    }

    @Test
    public void factoryCreate_throwIllegalArgumentException_ifBadShapeNameWasDone(){
        String name = "Circle";
        Throwable exception = assertThrows(IllegalArgumentException.class, ()->factory.createShape(name, "5"));
        assertEquals("There is no shape: " + name, exception.getMessage());
    }

    @Test
    public void factoryCreate_throwIllegalArgumentException_ifNotTriangleInequality(){
        Throwable exception = assertThrows(IllegalArgumentException.class, ()->factory.createShape("TRIANGLE", "100 5 5"));
        assertEquals("Impossible triangle's sides (every side must be less than sum of another ones)", exception.getMessage());
    }

    @Test
    public void factoryCreate_throwIllegalArgumentException_ifTriangleNegativeSides(){
        Throwable exception = assertThrows(IllegalArgumentException.class, ()->factory.createShape("TRIANGLE", "-5 5 5"));
        assertEquals("Triangle's sides cannot be negative", exception.getMessage());
    }

    @Test
    public void factoryCreate_throwIllegalArgumentException_ifRectangleNegativeSides(){
        Throwable exception = assertThrows(IllegalArgumentException.class, ()->factory.createShape("RECTANGLE", "-5 5"));
        assertEquals("Rectangle's sides cannot be negative", exception.getMessage());
    }

    @Test
    public void factoryCreate_throwIllegalArgumentException_ifCircleNegativeRadius(){
        Throwable exception = assertThrows(IllegalArgumentException.class, ()->factory.createShape("CIRCLE", "-5"));
        assertEquals("Radius cannot be negative", exception.getMessage());
    }


}
