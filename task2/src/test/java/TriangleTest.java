import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.cft.focusStar.leonova.Shapes.AdditionalInfoHolder;
import ru.cft.focusStar.leonova.Shapes.Triangle;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.closeTo;

public class TriangleTest {

    @Test
    public void triangleCheckArea(){
        Triangle triangle = new Triangle(5.0, 4.0, 3.0);
        assertThat(triangle.getArea(), is(closeTo(6.0, 0.01)));
    }

    @Test
    public void triangleCheckPerimeter(){
        Triangle triangle = new Triangle(5.0, 4.0, 3.0);
        assertThat(triangle.getPerimeter(), is(closeTo(12.0, 0.01)));
    }

    @Test
    public void triangleCheckAside() {
        Triangle triangle = new Triangle(5.0, 4.0, 3.0);
        for(AdditionalInfoHolder info : triangle.getAdditionalCharacteristics()){
            if(info.getCharacteristicName().equals("Угол напротив стороны с длиной 5.0")){
                assertThat(info.getCharacteristicValue(), is(closeTo(90.0, 0.01)));
                return;
            }
        }
        Assertions.fail("Not found characteristicInfo with \"Угол напротив стороны с длиной 5.0\" name");
    }

    @Test
    public void triangleCheckBside() {
        Triangle triangle = new Triangle(5.0, 4.0, 3.0);
        for(AdditionalInfoHolder info : triangle.getAdditionalCharacteristics()){
            if(info.getCharacteristicName().equals("Угол напротив стороны с длиной 4.0")){
                assertThat(info.getCharacteristicValue(), is(closeTo(53.13, 0.01)));
                return;
            }
        }
        Assertions.fail("Not found characteristicInfo with \"Угол напротив стороны с длиной 4.0\" name");
    }

    @Test
    public void triangleCheckCside() throws Exception {
        Triangle triangle = new Triangle(5.0, 4.0, 3.0);
        for(AdditionalInfoHolder info : triangle.getAdditionalCharacteristics()){
            if(info.getCharacteristicName().equals("Угол напротив стороны с длиной 3.0")){
                assertThat(info.getCharacteristicValue(), is(closeTo(36.87, 0.01)));
                return;
            }
        }
        Assertions.fail("Not found characteristicInfo with \"Угол напротив стороны с длиной 3.0\" name");
    }
}
