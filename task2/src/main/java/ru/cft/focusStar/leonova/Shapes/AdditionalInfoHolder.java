package ru.cft.focusStar.leonova.Shapes;

public class AdditionalInfoHolder {

    private String characteristicName;
    private double characteristicValue;

    public AdditionalInfoHolder(String characteristicName, double characteristicValue){
        this.characteristicName = characteristicName;
        this.characteristicValue = characteristicValue;
    }

    public String getCharacteristicName() {
        return characteristicName;
    }

    public double getCharacteristicValue() {
        return characteristicValue;
    }
}
