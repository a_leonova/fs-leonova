package ru.cft.focusStar.leonova.ShapeFactory;

import ru.cft.focusStar.leonova.Shapes.*;

public class ShapeProvider {

    public void addShapesToFactory(Factory factory){
        factory.addShapeCreator("CIRCLE", new ShapeCreator() {
            @Override
            public Shape createShape(Double[] args) {
                if(args.length != 1){
                    throw new IllegalArgumentException("Bad number of characteristic: need 1");
                }
                return new Circle(args[0]);
            }
        });

        factory.addShapeCreator("RECTANGLE", new ShapeCreator() {
            @Override
            public Shape createShape(Double[] args) {
                if (args.length != 2){
                    throw new IllegalArgumentException("Bad number of characteristic: need 2");
                }
                return new Rectangle(args[0], args[1]);
            }
        });

        factory.addShapeCreator("TRIANGLE", new ShapeCreator() {
            @Override
            public Shape createShape(Double[] args) {
                if(args.length != 3){
                    throw new IllegalArgumentException("Bad number of characteristic: need 3");
                }
                return new Triangle(args[0], args[1], args[2]);
            }
        });
    }
}
