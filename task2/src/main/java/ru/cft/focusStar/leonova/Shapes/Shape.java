package ru.cft.focusStar.leonova.Shapes;

import java.util.ArrayList;

public interface Shape  {

    String getName();

    double getArea();

    double getPerimeter();

    ArrayList<AdditionalInfoHolder> getAdditionalCharacteristics();


}
