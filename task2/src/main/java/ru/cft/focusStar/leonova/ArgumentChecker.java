package ru.cft.focusStar.leonova;

public class ArgumentChecker {

    public static void checkArgumentCount(int count){
        if(count < 1){
            System.err.println("Write input file name");
            throw new IllegalArgumentException("No input file");
        }
        if(count > 2){
            System.err.println("Too many arguments. It should be: path/filenameIN [path/filenameOUT]");
            throw  new IllegalArgumentException("Too many arguments");
        }
    }
}
