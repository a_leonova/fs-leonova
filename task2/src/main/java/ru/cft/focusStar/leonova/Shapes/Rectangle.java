package ru.cft.focusStar.leonova.Shapes;

import java.util.ArrayList;

public class Rectangle implements Shape {



    private static final String NAME = "Прямоугольник";
    private double a;
    private double b;


    public Rectangle(Double a, Double b){
        if(a < 0 || b < 0){
            throw new IllegalArgumentException("Rectangle's sides cannot be negative");
        }
        this.a = a;
        this.b = b;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public double getArea() {
        return a * b;
    }

    @Override
    public double getPerimeter() {
        return 2 * (a + b);
    }

    @Override
    public ArrayList<AdditionalInfoHolder> getAdditionalCharacteristics() {
        ArrayList<AdditionalInfoHolder> info = new ArrayList<>();

        info.add(new AdditionalInfoHolder("Диагональ", countDiagonal()));
        info.add(new AdditionalInfoHolder("Ширина", Double.min(a, b)));
        info.add(new AdditionalInfoHolder("Длина", Double.max(a, b)));
        return info;
    }

    private double countDiagonal(){
        return Math.sqrt(a * a + b * b);
    }

}
