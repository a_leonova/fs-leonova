package ru.cft.focusStar.leonova.Shapes;

public abstract class ShapeCreator {

    public abstract Shape createShape(Double[] args);
}
