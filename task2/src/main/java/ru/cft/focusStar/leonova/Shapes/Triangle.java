package ru.cft.focusStar.leonova.Shapes;

import java.util.ArrayList;

public class Triangle implements Shape {

    private static final String NAME = "Треугольник";
    private double a;
    private double b;
    private double c;

    public Triangle(){}

    public Triangle(Double a, Double b, Double c){
        if(a < 0 || b < 0 || c < 0){
            throw  new IllegalArgumentException("Triangle's sides cannot be negative");
        }
        if(!isGoodSides(a, b, c)){
            throw new IllegalArgumentException("Impossible triangle's sides (every side must be less than sum of another ones)");
        }
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public double getArea() {
        double halfPerimeter = getPerimeter() / 2;
        return Math.sqrt(halfPerimeter * (halfPerimeter - a) * (halfPerimeter - b) * (halfPerimeter - c));
    }

    @Override
    public double getPerimeter() {
        return a + b + c;
    }

    @Override
    public ArrayList<AdditionalInfoHolder> getAdditionalCharacteristics() {
        ArrayList<AdditionalInfoHolder> info = new ArrayList<>();
        double acosRad = Math.acos(findCos(b, c, a));
        info.add( new AdditionalInfoHolder("Угол напротив стороны с длиной "+ a , Math.toDegrees(acosRad)));
        acosRad = Math.acos(findCos(a, c, b));
        info.add( new AdditionalInfoHolder("Угол напротив стороны с длиной "+ b, Math.toDegrees(acosRad)));
        acosRad = Math.acos(findCos(b, a, c));
        info.add( new AdditionalInfoHolder("Угол напротив стороны с длиной " + c , Math.toDegrees(acosRad)));
        return info;
    }

    private double findCos(double nearSide1, double nearSide2, double oppositeSide){
        return (nearSide1 * nearSide1 + nearSide2 * nearSide2 - oppositeSide * oppositeSide) / (2 * nearSide1 * nearSide2);
    }

    private boolean isGoodSides(double a, double b, double c){
        return a < b + c && b < c + a && c < a + b;
    }
}
