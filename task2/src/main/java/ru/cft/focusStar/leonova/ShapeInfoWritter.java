package ru.cft.focusStar.leonova;

import ru.cft.focusStar.leonova.Shapes.AdditionalInfoHolder;
import ru.cft.focusStar.leonova.Shapes.Shape;

import java.io.PrintWriter;
import java.util.ArrayList;

public class ShapeInfoWritter {

    PrintWriter writter;
    public ShapeInfoWritter(PrintWriter writter){
        this.writter = writter;
    }

    public void writeInfo(Shape shape){
        writter.format("Тип фигуры: %s\n", shape.getName());
        writter.format("Площадь: %.2f\n", shape.getArea());
        writter.format("Периметр: %.2f\n", shape.getPerimeter());
        ArrayList<AdditionalInfoHolder> additionalInfo = shape.getAdditionalCharacteristics();
        for(AdditionalInfoHolder info : additionalInfo){
            writter.format("%s: %.2f\n", info.getCharacteristicName(), info.getCharacteristicValue());

        }
    }

    public void setWritter(PrintWriter writter) {
        this.writter = writter;
    }
}
