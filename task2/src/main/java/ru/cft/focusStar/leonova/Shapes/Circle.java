package ru.cft.focusStar.leonova.Shapes;

import java.util.ArrayList;


public class Circle implements Shape {

    private static final String NAME = "Круг";
    private double radius;

    public Circle(Double radius){
        if (radius < 0){
            throw new IllegalArgumentException("Radius cannot be negative");
        }
        this.radius = radius;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public double getArea(){
        return Math.PI * Math.pow(radius, 2);
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public ArrayList<AdditionalInfoHolder> getAdditionalCharacteristics() {
        ArrayList<AdditionalInfoHolder> info = new ArrayList<>();
        info.add(new AdditionalInfoHolder("Радиус", radius));
        info.add(new AdditionalInfoHolder("Диаметр", 2 * radius));
        return info;
    }

}
