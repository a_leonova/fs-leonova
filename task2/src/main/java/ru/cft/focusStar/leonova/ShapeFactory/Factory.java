package ru.cft.focusStar.leonova.ShapeFactory;

import ru.cft.focusStar.leonova.Shapes.*;

import java.util.HashMap;

public class Factory {

    private HashMap<String, ShapeCreator> data = new HashMap<>();


    public Shape createShape(String name, String args){
        ShapeCreator shapeCreator = data.get(name);
        if(shapeCreator == null){
            throw new IllegalArgumentException("There is no shape: " + name);
        }
        return data.get(name).createShape(parseCharacteristics(args));
    }

    public void addShapeCreator(String name, ShapeCreator creator){
        data.put(name, creator);
    }

    private Double[] parseCharacteristics(String args){
        String[] numbers = args.split(" ");
        Double[] retVal = new Double[numbers.length];
        for(int i = 0; i < numbers.length; ++i){
            retVal[i] = Double.parseDouble(numbers[i]);
        }
        return retVal;
    }



}
