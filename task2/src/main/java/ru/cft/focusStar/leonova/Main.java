package ru.cft.focusStar.leonova;

import ru.cft.focusStar.leonova.ShapeFactory.Factory;
import ru.cft.focusStar.leonova.ShapeFactory.ShapeProvider;
import ru.cft.focusStar.leonova.Shapes.Shape;
import java.io.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        try{
            ArgumentChecker.checkArgumentCount(args.length);
        }catch (IllegalArgumentException e){
            LOGGER.error(e.getMessage());
            return;
        }

        LOGGER.info("Start program");

        String inputFileName = args[0];
        String shapeName;
        String shapeParameters;
        try(BufferedReader reader = new BufferedReader(new FileReader(inputFileName))) {
            LOGGER.info("Reader was created");
            shapeName = reader.readLine();
            shapeParameters = reader.readLine();
        } catch (FileNotFoundException e) {
            LOGGER.error("Couldn't open file:{}\nMessage:{}", inputFileName, e);
            System.err.println("Couldn't open file");
            return;
        } catch (IOException e) {
            LOGGER.error("Couldn't read or close file:{}\nMessage: {}", inputFileName, e);
            System.err.println("Couldn't read or close file");
            return;
        }

        Shape shape;
        try {
            shape = createShape(shapeName, shapeParameters);
            LOGGER.info("Shape was created");
        }catch (IllegalArgumentException e){
            LOGGER.error("Exception in factory.createShape({}, {})\nMessage: {}", shapeName, shapeParameters, e);
            System.err.println(e.getMessage());
            return;
        }

        writeShapeInfo(shape, args);
        LOGGER.info("End program");
    }

    private static void writeShapeInfo(Shape shape, String[] args){
        PrintWriter standardOutputWriter = new PrintWriter(System.out);

        LOGGER.info("Writer for output was created: stdout");
        ShapeInfoWritter shapeInfoWritter = new ShapeInfoWritter(standardOutputWriter);
        if(args.length == 2){
            String outputFileName = args[1];
            try(PrintWriter outputFileWriter = new PrintWriter(new FileWriter(outputFileName))) {
                LOGGER.info("Writer for output was changed: {}", args[1]);
                shapeInfoWritter.setWritter(outputFileWriter);
                shapeInfoWritter.writeInfo(shape);
            }catch (FileNotFoundException e){
                LOGGER.error("Exception during opening outputFile:{}\nMessage: {} ", outputFileName, e);
                System.err.println("Couldn't open output file. Result will be written into stdout (" + e + ")");
            }
            catch (IOException e) {
                LOGGER.error("Exception during working with outputFile:{}\nMessage: {} ", outputFileName, e);
            }
        }
        else{
            shapeInfoWritter.writeInfo(shape);
            standardOutputWriter.flush();
        }
    }

    private static Shape createShape(String name, String args){
        Factory factory = new Factory();
        ShapeProvider figureProvider = new ShapeProvider();
        figureProvider.addShapesToFactory(factory);

        return factory.createShape(name, args);
    }
}
