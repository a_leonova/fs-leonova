import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Producer implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(Producer.class);
    private int timeToWork;
    private TaskPool taskPool;

    public Producer(int timeToWork, TaskPool taskPool) {
        this.timeToWork = timeToWork;
        this.taskPool = taskPool;
    }

    @Override
    public void run() {
        while(!Thread.currentThread().isInterrupted()){
            try {
                LOGGER.info("started creating resource");
                Thread.sleep(timeToWork);
                Resource resource = new Resource();
                LOGGER.info("created resource {}", resource.getUuid());
                taskPool.addResource(resource);
            } catch (InterruptedException e) {
                LOGGER.info("Thread got interrupted and finishing");
                return;
            }
        }
        LOGGER.info("Thread now finish working");
    }
}
