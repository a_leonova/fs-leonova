import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Main {
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        PropertyWorker propertyWorker;
        int producersNumber;
        int consumersNumber;
        int producerTime;
        int consumerTime;
        int size;

        try {
            propertyWorker = new PropertyWorker();
            producersNumber = getPropertyAndCheck("N", propertyWorker);
            consumersNumber = getPropertyAndCheck("M", propertyWorker);
            producerTime = getPropertyAndCheck("tN", propertyWorker);
            consumerTime = getPropertyAndCheck("tM", propertyWorker);
            size = getPropertyAndCheck("S", propertyWorker);
        } catch (Exception e) {
            LOGGER.info("Unable to parse properties: ", e);
            return;
        }

        TaskPool taskPool = new TaskPool(size);
        ArrayList<Thread> threads = new ArrayList<>(producersNumber + consumersNumber);
        for(int i = 0; i < producersNumber; ++i){
            threads.add(new Thread(new Producer(producerTime, taskPool), "Producer #" + i));
        }
        for(int i = 0; i < consumersNumber; ++i){
            threads.add(new Thread(new Consumer(consumerTime, taskPool), "Consumer #" + i));
        }
        for(Thread thread : threads){
            thread.start();
        }

        System.out.println("PRESS ENTER TO FINISH");
        try {
            System.in.read();
            finish(threads);
        } catch (IOException e) {
            LOGGER.info("Can't read from system.in", e);
        }

    }

    private static int getPropertyAndCheck(String propName, PropertyWorker propertyWorker){
        Integer value = propertyWorker.getIntProp(propName);
        if(value <= 0){
            LOGGER.info("Bad value value: {}", value);
            throw  new NumberFormatException("Negative value is impossible");
        }
        return value;
    }

    public static void finish(List<Thread> threads){
        for(Thread thread : threads){
            thread.interrupt();
        }
        try{
            for(Thread thread : threads){
                thread.join();
            }
        } catch (InterruptedException e){
            LOGGER.info("MAIN THREAD INTERRUPTED", e);
        }
    }
}
