import java.util.UUID;

public class Resource {
    private UUID uuid = UUID.randomUUID();

    public UUID getUuid() {
        return uuid;
    }

}
