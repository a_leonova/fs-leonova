import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Consumer implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(Consumer.class);

    private int timeToWork;
    private TaskPool taskPool;

    public Consumer(int timeToWork, TaskPool taskPool) {
        this.timeToWork = timeToWork;
        this.taskPool = taskPool;
    }

    @Override
    public void run() {
        while(!Thread.currentThread().isInterrupted()){
            try {
                Resource resource = taskPool.getResource();
                LOGGER.info("got resource {}", resource.getUuid());
                LOGGER.info("started consuming resource");
                Thread.sleep(timeToWork);
            } catch (InterruptedException e) {
                LOGGER.info("Thread got interrupted and finishing");
                return;
            }
        }
        LOGGER.info("Thread was interrupted and now finish working");
    }
}
