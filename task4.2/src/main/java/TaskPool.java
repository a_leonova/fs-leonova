import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.Queue;

public class TaskPool {
    private static final Logger LOGGER = LoggerFactory.getLogger(TaskPool.class);
    private final Queue<Resource> resources;
    private int size;

    public TaskPool(int size){
        this.size = size;
        resources = new LinkedList<>();
    }

    public void addResource(Resource resource) throws InterruptedException {
        synchronized (resources){
            LOGGER.debug("Thread {} wants to add resource", Thread.currentThread().getName());
            while(resources.size() == size){
                LOGGER.debug("Thread {} waiting for empty space", Thread.currentThread().getName());
                resources.wait();
            }
            resources.add(resource);
            LOGGER.debug("Thread {} add resource. TaskPool size is {}", Thread.currentThread().getName(), resources.size());
            resources.notifyAll();
        }
    }

    public Resource getResource() throws InterruptedException {
        synchronized (resources){
            LOGGER.debug("Thread {} wants to consume resource", Thread.currentThread().getName());
            while (resources.size() == 0){
                LOGGER.debug("Thread {} waiting for resource", Thread.currentThread().getName());
                resources.wait();
            }
            Resource r = resources.poll();
            LOGGER.debug("Thread {} got resource. Taskpool size is {}", Thread.currentThread().getName(), resources.size());
            resources.notifyAll();
            return r;
        }
    }
}
