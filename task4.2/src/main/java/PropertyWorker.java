import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
public class PropertyWorker {
    private Properties prop = new Properties();
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyWorker.class);

    public PropertyWorker() throws IOException {
        String propFileName = "config.properties";
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName)) {
            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }
        }
    }


    public Integer getIntProp(String propName) {
        int value;
        String portValue = prop.getProperty(propName);
        value = Integer.parseInt(portValue);
        return value;
    }

}
