package server;

import common.Message;
import common.RequestType;
import common.ResponseType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientHandler implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientHandler.class);

    private Socket clientSocket;
    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;

    private Sender sender;
    private UserRepository usersRepo;
    private ConnectionRepository connections;

    private boolean work;


    public ClientHandler(Socket clientSocket, Sender sender,
                         UserRepository usersRepo, ConnectionRepository connectionRepository) throws IOException {
        this.clientSocket = clientSocket;
        this.sender = sender;
        this.usersRepo = usersRepo;
        connections = connectionRepository;

        try{
            dataInputStream = new DataInputStream(clientSocket.getInputStream());
            dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());
            connections.addSocketStreams(clientSocket, dataOutputStream, dataInputStream);
            work = true;
        } catch (IOException e){
            LOGGER.info("Couldn't open stream. Closing all socket's things: ", e);
            closeSocketStuffs();
            throw e;
        }
    }

    @Override
    public void run() {
        try{
            while (work){
                RequestType requestType = RequestType.valueOf(dataInputStream.readUTF());
                switch (requestType){
                    case MESSAGE:
                        messageHandle();
                        break;
                    case LOGIN:
                        loginHandle();
                        break;
                    case LOGOUT:
                        logoutHandle();
                        break;
                }
            }
        } catch (IOException e) {
            LOGGER.info("Couldn't use inputStream: ", e);
            deleteUserInfoOnError();
            closeSocketStuffs();
        } catch (IllegalArgumentException e){
            LOGGER.info("Bad request type: ", e);
        }
    }

    private void messageHandle() throws IOException {
        LOGGER.debug("Message request");
        String message = dataInputStream.readUTF();
        String name = usersRepo.getName(clientSocket);
        sender.sendMessageEveryone(new Message(name, message));
    }

    private void loginHandle() throws IOException {
        LOGGER.debug("Login request");
        String name = dataInputStream.readUTF();
        if(usersRepo.addUser(name, clientSocket)){
            LOGGER.debug(name + ": successful chat joining");
            sender.newUserAdded(clientSocket);
            sender.successLogin(clientSocket);
        }
        else{
            LOGGER.debug(name + ": busy name error");
            sender.sendError(ResponseType.BUSY_NAME, clientSocket);
        }
    }

    private void logoutHandle(){
        String name = usersRepo.logoutUser(clientSocket);
        LOGGER.debug(name + ": logout");
        work = false;
        connections.deleteSocketStreams(clientSocket);
        closeSocketStuffs();
        sender.userRemoved(name);
    }

    private void deleteUserInfoOnError(){
        usersRepo.logoutUser(clientSocket);
        connections.deleteSocketStreams(clientSocket);
    }

    private void closeSocketStuffs(){
        if(dataOutputStream != null){
            try{
                dataOutputStream.close();
            } catch (IOException e){
                LOGGER.info("Exception during closing dataoutput: ", e);
            }
        }
        if(dataInputStream != null){
            try{
                dataInputStream.close();
            } catch (IOException e){
                LOGGER.info("Exception during closing datainput: ", e);
            }
        }
        usersRepo.logoutUser(clientSocket);
        connections.deleteSocketStreams(clientSocket);
        try{
            clientSocket.close();
        } catch (IOException e){
            LOGGER.info("Exception during closing socket: ", e);
        }
    }
}
