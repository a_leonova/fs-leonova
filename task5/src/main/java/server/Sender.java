package server;

import common.Message;
import common.ResponseType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

public class Sender {
    private static final Logger LOGGER = LoggerFactory.getLogger(Sender.class);

    private UserRepository userRepository;
    private ConnectionRepository connectionRepository;

    public Sender(UserRepository userRepository, ConnectionRepository connectionRepository) {
        this.userRepository = userRepository;
        this.connectionRepository = connectionRepository;
    }

    public void newUserAdded(Socket addedUserSocket){
        String name = userRepository.getName(addedUserSocket);
        Set<Socket> addresses = userRepository.getOnlineUsersSocket();
        Iterator<Socket> iterator = addresses.iterator();
        while (iterator.hasNext()){
            Socket socket = iterator.next();
            //don't sending newUser info, that it was added. He will get list of all online users with himself
            if(socket.equals(addedUserSocket)){
                continue;
            }
            DataOutputStream dos = connectionRepository.getSocketOut(socket);
            try{
                dos.writeUTF(ResponseType.NEW_USER.name());
                dos.writeUTF(name);
                dos.flush();
            } catch (IOException e) {
                LOGGER.info("Exception during sending: ", e);
                closeSocketStuffs(socket);
            }
        }
    }

    public void userRemoved(String name){
        Set<Socket> addresses = userRepository.getOnlineUsersSocket();
        Iterator<Socket> iterator = addresses.iterator();
        while (iterator.hasNext()){
            Socket socket = iterator.next();
            DataOutputStream dos = connectionRepository.getSocketOut(socket);
            try{
                dos.writeUTF(ResponseType.REMOVE_USER.name());
                dos.writeUTF(name);
                dos.flush();
            } catch (IOException e) {
                LOGGER.info("Exception during sending: ", e);
                closeSocketStuffs(socket);
            }
        }
    }

    public void sendMessageEveryone(Message message){
        Set<Socket> addresses = userRepository.getOnlineUsersSocket();
        Iterator<Socket> iterator = addresses.iterator();
        while (iterator.hasNext()){
            Socket socket = iterator.next();
            DataOutputStream dos = connectionRepository.getSocketOut(socket);
            try{
                dos.writeUTF(ResponseType.MESSAGE.name());
                dos.writeUTF(message.getMessageAuthor());
                dos.writeUTF(message.getMessageText());
                dos.flush();
            } catch (IOException e) {
                LOGGER.info("Exception during sending: ", e);
                closeSocketStuffs(socket);
            }
        }
    }

    public void successLogin(Socket socket){
        try{
            DataOutputStream dos = connectionRepository.getSocketOut(socket);
            dos.writeUTF(ResponseType.SUCCESS_LOGIN.name());

            Collection<String> onlineUsers = userRepository.getOnlineUsers();
            dos.writeInt(onlineUsers.size());
            for(String name : onlineUsers){
               dos.writeUTF(name);
            }
            dos.flush();
        } catch (IOException e) {
            LOGGER.info("Exception during sending: ", e);
            closeSocketStuffs(socket);
        }
    }

    public void sendError(ResponseType responseType, Socket socket){
        try{
            DataOutputStream dos = connectionRepository.getSocketOut(socket);
            dos.writeUTF(responseType.name());
            dos.flush();
        } catch (IOException e) {
            LOGGER.info("Exception during sending: ", e);
            closeSocketStuffs(socket);
        }
    }

    private void closeSocketStuffs(Socket socket) {
        try {
            connectionRepository.getSocketIn(socket).close();
        } catch (IOException e) {
            LOGGER.info("Couldn't close input stream: ", e);
        }
        try {
            connectionRepository.getSocketOut(socket).close();
        } catch (IOException e) {
            LOGGER.info("Couldn't close output stream: ", e);
        }
        userRepository.logoutUser(socket);
        connectionRepository.deleteSocketStreams(socket);
        try{
            socket.close();
        } catch (IOException e){
            LOGGER.info("Couldn't close socket: ", e);
        }
    }
}
