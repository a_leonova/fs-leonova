package server;

import common.PropertiesReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        PropertiesReader propertiesReader = new PropertiesReader();
        Integer port = propertiesReader.getIntProp("port");
        if(port == null){
            return;
        }

        try {
            Server server = new Server(port);
            server.go();
        } catch (IOException e) {
            LOGGER.info("Couldn't open server socket: {}", e);
            return;
        }

    }
}
