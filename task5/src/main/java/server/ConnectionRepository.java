package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConnectionRepository {
    private final Map<Socket, DataInputStream> inputStreams = new ConcurrentHashMap<>();
    private final Map<Socket, DataOutputStream> outputStreams = new ConcurrentHashMap<>();

    public void addSocketStreams(Socket socket, DataOutputStream dataOutputStream, DataInputStream dataInputStream){
        inputStreams.put(socket, dataInputStream);
        outputStreams.put(socket, dataOutputStream);
    }

    public DataOutputStream getSocketOut(Socket socket){
        return outputStreams.get(socket);
    }

    public DataInputStream getSocketIn(Socket socket){
        return inputStreams.get(socket);
    }

    public void deleteSocketStreams(Socket socket){
        inputStreams.remove(socket);
        outputStreams.remove(socket);
    }

}
