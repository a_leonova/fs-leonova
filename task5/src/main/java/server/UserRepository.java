package server;

import java.net.Socket;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class UserRepository {
    private final Set<String> busyNames = new HashSet<>();
    private final Map<Socket, String> onlineUsers = new ConcurrentHashMap<>();


    public boolean addUser(String name, Socket socket){
        synchronized (busyNames){
            if(busyNames.add(name)){
                onlineUsers.put(socket, name);
                return true;
            }
            return false;
        }
    }

    public String getName(Socket socket){
        return onlineUsers.get(socket);
    }

    public String logoutUser(Socket socket){
        synchronized (busyNames){
            String name = onlineUsers.get(socket);
            busyNames.remove(name);
            onlineUsers.remove(socket);
            return name;
        }
    }

    public Set<Socket> getOnlineUsersSocket() {
        return onlineUsers.keySet();
    }

    public Collection<String> getOnlineUsers(){
        return onlineUsers.values();
    }


}
