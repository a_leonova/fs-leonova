package server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private static final Logger LOGGER = LoggerFactory.getLogger(Server.class);

    private ServerSocket serverSocket;
    private UserRepository userRepository = new UserRepository();
    private ConnectionRepository connectionRepository = new ConnectionRepository();
    private Sender sender = new Sender(userRepository, connectionRepository);

    public Server(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }


    public void go()  {

        while(true){
            try{
                Socket socket = serverSocket.accept();
                ClientHandler clientHandler = new ClientHandler(socket, sender, userRepository, connectionRepository);
                Thread thread = new Thread(clientHandler);
                thread.start();
            } catch (IOException e){
                LOGGER.info("Couldn't create connection: ", e);
            }
        }
    }



}
