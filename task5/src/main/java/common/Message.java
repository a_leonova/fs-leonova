package common;

public class Message {
    private String messageAuthor;
    private String messageText;

    public Message(String messageAuthor, String messageText) {
        this.messageAuthor = messageAuthor;
        this.messageText = messageText;
    }

    public String getMessageText() {
        return messageText;
    }

    public String getMessageAuthor() {
        return messageAuthor;
    }
}
