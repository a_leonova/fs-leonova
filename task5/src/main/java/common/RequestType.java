package common;

public enum  RequestType {
    //MESSAGE data
    MESSAGE,
    //LOGOUT
    LOGOUT,
    //LOGIN name
    LOGIN
}
