package common;

public enum ResponseType {
    //MESSAGE name data
    MESSAGE,
    //NEW_USER userName
    NEW_USER,
    //REMOVE_USER userName
    REMOVE_USER,
    //SUCCESS_LOGIN count:int onlineUsers:List<String>
    SUCCESS_LOGIN,
    BUSY_NAME
}
