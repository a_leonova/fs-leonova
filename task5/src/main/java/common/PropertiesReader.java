package common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class PropertiesReader {
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesReader.class);

    public Integer getIntProp(String propName) {
        int value;
        try {
            String portValue = getPropValue(propName);
            if(portValue == null){
                LOGGER.info("There is no property with \"" + propName + "\" name");
                return null;
            }
            value = Integer.parseInt(portValue);
            return value;
        } catch (IOException e) {
            LOGGER.info("Couldn't work with property file: ", e);
            return null;
        } catch (NumberFormatException e){
            LOGGER.info("Bad port value: ", e);
            return null;
        }
    }

    private String getPropValue(String propName) throws IOException {
        Properties prop = new Properties();
        String propFileName = "config.properties";
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName)) {
            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }
            String value = prop.getProperty(propName);
            return value;
        }
    }
}
