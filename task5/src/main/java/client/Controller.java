package client;

import client.listeners.ConnectListener;
import client.listeners.LogoutListener;
import client.listeners.SendMessageListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Controller implements LogoutListener, ConnectListener, SendMessageListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(Controller.class);
    private NetWorker netWorker;
    private int port;

    public Controller(NetWorker netWorker, int port) {
        this.netWorker = netWorker;
        this.port = port;
    }

    @Override
    public void connect(String serverAddress, String name) {
        try {
            netWorker.openSocket(serverAddress, port);
            new Thread(()->netWorker.runReceive()).start();
            netWorker.login(name);
        } catch (IOException e) {
            LOGGER.info("Couldn't create connection");
        }
    }

    @Override
    public void logout() {
        netWorker.logout();
    }

    @Override
    public void sendMessage(String message) {
        netWorker.sendMessage(message);
    }
}
