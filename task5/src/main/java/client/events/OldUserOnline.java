package client.events;

public class OldUserOnline {
    private String userName;

    public OldUserOnline(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }
}
