package client.events;

public class UserLeft {
    private String userName;

    public UserLeft(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }
}
