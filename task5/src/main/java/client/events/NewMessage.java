package client.events;

import common.Message;

public class NewMessage {
    private Message message;

    public NewMessage(Message message) {
        this.message = message;
    }

    public Message getMessage() {
        return message;
    }
}
