package client.events;

import java.util.List;

public class SuccessLogin {
    List<String> onlineUsers;

    public SuccessLogin(List<String> onlineUsers) {
        this.onlineUsers = onlineUsers;
    }

    public List<String> getOnlineUsers() {
        return onlineUsers;
    }
}
