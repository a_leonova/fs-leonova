package client.events;

public class UserJoin {
    private String userName;

    public UserJoin(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }
}
