package client;

import client.view.View;
import common.PropertiesReader;

public class Main {

    public static void main(String[] args) {
        PropertiesReader propertiesReader = new PropertiesReader();
        Integer port = propertiesReader.getIntProp("port");
        if(port == null){
            return;
        }

        View view = new View();
        NetWorker netWorker = new NetWorker();
        netWorker.addObserver(view);
        Controller controller = new Controller(netWorker, port);
        view.setNewMessageListener(controller);
        view.setConnectListener(controller);
        view.setLogoutListener(controller);
        view.startWindowShow(true);

    }
}
