package client.myObserver;

import java.util.Observer;

public interface MyObservable {
    void addObserver(MyObserver observer);
    void removeObserver(MyObserver observer);
}
