package client.myObserver;

import client.events.*;

public interface MyObserver {
    void update(BusyName event);
    void update(NewMessage event);
    void update(SuccessLogin event);
    void update(UserJoin event);
    void update(UserLeft event);
    void update(BadServerAddress event);
    void update(SocketProblem event);
    void update(OldUserOnline event);
}
