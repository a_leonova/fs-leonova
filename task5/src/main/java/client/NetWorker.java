package client;

import client.events.*;
import client.myObserver.MyObservable;
import client.myObserver.MyObserver;
import common.Message;
import common.RequestType;
import common.ResponseType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class NetWorker implements MyObservable {
    private static final Logger LOGGER = LoggerFactory.getLogger(NetWorker.class);
    private static final int TIME_TO_SLEEP = 200;

    private List<MyObserver> observers = new ArrayList<>();

    private Socket socket;
    private volatile boolean work;
    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;

    private List<String> usersOnline = new ArrayList<>();

    public void openSocket(String address, int port) throws IOException {
        try{
            socket = new Socket(address, port);
            dataInputStream = new DataInputStream(socket.getInputStream());
            dataOutputStream = new DataOutputStream(socket.getOutputStream());
            work = true;
        } catch (UnknownHostException e){
            BadServerAddress badServerAddress = new BadServerAddress();
            for (MyObserver observer : observers) {
                observer.update(badServerAddress);
            }
            throw e;
        } catch (IOException e){
            SocketProblem socketProblem = new SocketProblem();
            for (MyObserver observer : observers) {
                observer.update(socketProblem);
            }
            throw e;
        }
    }

    public void login(String name){
        try{
            dataOutputStream.writeUTF(RequestType.LOGIN.name());
            dataOutputStream.writeUTF(name);
            dataOutputStream.flush();
        } catch (IOException e) {
            closeSocketStreams();
            SocketProblem socketProblem = new SocketProblem();
            for (MyObserver observer : observers) {
                observer.update(socketProblem);
            }
        }
    }

    public void sendMessage(String message) {
        try{
            dataOutputStream.writeUTF(RequestType.MESSAGE.name());
            dataOutputStream.writeUTF(message);
            dataOutputStream.flush();
        } catch (IOException e) {
            closeSocketStreams();
            SocketProblem socketProblem = new SocketProblem();
            for (MyObserver observer : observers) {
                observer.update(socketProblem);
            }
        }
    }

    public void logout() {
        try{
            LOGGER.debug("LOGOUT COME");
            dataOutputStream.writeUTF(RequestType.LOGOUT.name());
            dataOutputStream.flush();
            work = false;
            LOGGER.debug("SEND SERVER");
        } catch (IOException e) {
            closeSocketStreams();
            SocketProblem socketProblem = new SocketProblem();
            for (MyObserver observer : observers) {
                observer.update(socketProblem);
            }
        }
    }

    public void runReceive() {
        try{
            while (work){
                while(work && dataInputStream.available() <= 0){
                    Thread.sleep(TIME_TO_SLEEP);
                }
                if(work){
                    ResponseType responseType = ResponseType.valueOf(dataInputStream.readUTF());
                    switch (responseType){
                        case MESSAGE:
                            messageHandler();
                            break;
                        case SUCCESS_LOGIN:
                            successLoginHandler();
                            break;
                        case NEW_USER:
                            newUserHandler();
                            break;
                        case REMOVE_USER:
                            removeUserHandler();
                            break;
                        case BUSY_NAME:
                            busyNameHandler();
                            break;
                    }
                }
            }
            LOGGER.debug("Stop receiving");
        } catch (IOException e) {
            LOGGER.info("Exception during receiving: ", e);
            closeSocketStreams();
            SocketProblem socketProblem = new SocketProblem();
            for (MyObserver observer : observers) {
                observer.update(socketProblem);
            }
        } catch (IllegalArgumentException e){
            LOGGER.info("Bad response format: ", e);
        } catch (InterruptedException e) {
            LOGGER.info("Thread interrupted: ", e);
        }
    }

    @Override
    public void addObserver(MyObserver observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(MyObserver observer) {
        observers.remove(observer);
    }

    private void messageHandler() throws IOException {
        Message message = getMessage();
        NewMessage newMessage = new NewMessage(message);
        for (MyObserver observer : observers) {
            observer.update(newMessage);
        }
    }

    private Message getMessage() throws IOException {
        LOGGER.debug("Getting message");
        String name = dataInputStream.readUTF();
        String data = dataInputStream.readUTF();
        LOGGER.debug("SUCCESS Getting message");
        return new Message(name, data);
    }

    private void successLoginHandler() throws IOException {
        usersOnline.addAll(getUsersOnline());
        SuccessLogin successLogin = new SuccessLogin(usersOnline);
        for (MyObserver observer : observers) {
            observer.update(successLogin);
        }
    }

    private List<String> getUsersOnline() throws IOException {
        LOGGER.debug("Getting users online");
        int count = dataInputStream.readInt();
        List<String> usersOnline = new ArrayList<>(count);
        for(int i = 0; i < count; ++i){
            String name = dataInputStream.readUTF();
            usersOnline.add(name);
        }
        LOGGER.debug("SUCCESS Getting users online");

        for(String name : usersOnline){
            OldUserOnline oldUserOnline = new OldUserOnline(name);
            for (MyObserver observer : observers) {
                observer.update(oldUserOnline);
            }
        }
        return usersOnline;
    }

    private void newUserHandler() throws IOException {
        String newName = dataInputStream.readUTF();
        usersOnline.add(newName);
        UserJoin join = new UserJoin(newName);
        for (MyObserver observer : observers) {
            observer.update(join);
        }
    }

    private void removeUserHandler() throws IOException {
        String name = dataInputStream.readUTF();
        usersOnline.remove(name);
        UserLeft left = new UserLeft(name);
        for (MyObserver observer : observers) {
            observer.update(left);
        }
    }

    private void busyNameHandler() {
        BusyName busyName = new BusyName();
        for (MyObserver observer : observers) {
            observer.update(busyName);
        }
    }

    private void closeSocketStreams(){
        if(dataInputStream != null){
            try {
                dataInputStream.close();
            } catch (IOException e) {
                LOGGER.info("Couldn't close inputStream: ", e);
            }
        }
        if(dataOutputStream != null){
            try {
                dataOutputStream.close();
            } catch (IOException e) {
                LOGGER.info("Couldn't close outputStream: ", e);
            }
        }
        if(socket != null){
            try {
                socket.close();
            } catch (IOException e) {
                LOGGER.info("Couldn't close socket: ", e);
            }
        }
    }
}
