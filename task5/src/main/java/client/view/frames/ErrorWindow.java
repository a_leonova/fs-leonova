package client.view.frames;

import javax.swing.*;
import java.awt.*;

public class ErrorWindow {
    private JFrame jFrame;

    public ErrorWindow(String error, boolean fatal){
        String title = "Error";
        if(fatal){
            title = "Fatal error";
        }
        jFrame = new JFrame(title);
        jFrame.add(new JLabel(error), BorderLayout.CENTER);
        jFrame.pack();
        if(fatal){
            jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }
        jFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    public void setVisible(boolean value){
        jFrame.setVisible(value);
    }
}
