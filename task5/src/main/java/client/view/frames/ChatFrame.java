package client.view.frames;

import client.listeners.LogoutListener;
import client.listeners.SendMessageListener;
import client.view.panes.MessagePane;
import client.view.panes.OnlineUsersPane;
import common.Message;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class ChatFrame {

    private LogoutListener logoutListener;
    private JFrame chatFrame = new JFrame("Chat");
    private MessagePane messagePane = new MessagePane();
    private OnlineUsersPane onlineUsersPane = new OnlineUsersPane();

    public ChatFrame(){
        chatFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                logoutListener.logout();
                chatFrame.dispose();
            }
        });
        chatFrame.add(messagePane, BorderLayout.CENTER);
        chatFrame.add(onlineUsersPane, BorderLayout.WEST);
        chatFrame.pack();
        chatFrame.setLocationRelativeTo(null);
    }

    public void setVisible(boolean value){
        chatFrame.setVisible(value);
    }

    public void addMessage(Message message){
        messagePane.addMessage(message);
    }

    public void setNewMessageListener(SendMessageListener sendMessageListener) {
        messagePane.setSendMessageListener(sendMessageListener);
    }

    public void setLogoutListener(LogoutListener logoutListener) {
        this.logoutListener = logoutListener;
    }

    public void addUser(String name){
        onlineUsersPane.addUser(name);
    }

    public void removeUser(String name){
        onlineUsersPane.removeUser(name);
    }
}
