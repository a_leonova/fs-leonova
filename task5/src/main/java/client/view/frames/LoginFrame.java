package client.view.frames;

import client.listeners.ConnectListener;

import javax.swing.*;
import java.awt.*;

public class LoginFrame {
    private JFrame frame = new JFrame("Login");
    private ConnectListener connectListener;

    public LoginFrame() {
        JPanel jPanel = new JPanel();
        GridLayout form = new GridLayout(2,2);
        jPanel.setLayout(form);
        jPanel.add(new JLabel("Server address: "));
        JTextField addressTextField = new JTextField();
        jPanel.add(addressTextField);
        jPanel.add(new JLabel("Name: "));
        JTextField nameTextField = new JTextField();
        jPanel.add(nameTextField);
        frame.add(jPanel, BorderLayout.CENTER);
        JButton connectButton = new JButton("Connect");
        connectButton.addActionListener(e->connectListener.connect(addressTextField.getText(), nameTextField.getText()));
        frame.add(connectButton, BorderLayout.SOUTH);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
    }

    public void setVisible(boolean value){
        frame.setVisible(value);
    }

    public void dispose(){
        frame.dispose();
    }

    public void setConnectListener(ConnectListener connectListener) {
        this.connectListener = connectListener;
    }
}
