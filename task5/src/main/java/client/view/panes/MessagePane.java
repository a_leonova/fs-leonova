package client.view.panes;

import client.listeners.SendMessageListener;
import common.Message;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.*;

public class MessagePane extends JPanel {

    private JPanel mainList;
    private SendMessageListener sendMessageListener;

    public MessagePane() {
        setLayout(new BorderLayout());

        mainList = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1;
        gbc.weighty = 1;
        mainList.add(new JPanel(), gbc);

        add(new JScrollPane(mainList));

        JTextField messageTextField = new JTextField();
        JButton sendButton = new JButton("Send");
        sendButton.addActionListener(e -> {
            String text = messageTextField.getText();
            messageTextField.setText("");
            sendMessageListener.sendMessage(text);
        });

        JPanel messageCreatePanel = new JPanel(new GridLayout(2,1));
        messageCreatePanel.add(messageTextField);
        messageCreatePanel.add(sendButton);
        add(messageCreatePanel, BorderLayout.SOUTH);
    }

    public void setSendMessageListener(SendMessageListener sendMessageListener) {
        this.sendMessageListener = sendMessageListener;
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(200, 500);
    }

    public void addMessage(Message message){
        JPanel panel = new JPanel();
        panel.add(new JLabel("From: " + message.getMessageAuthor()));
        panel.add(new JLabel("Message: " + message.getMessageText()));
        panel.setBorder(new MatteBorder(0, 0, 1, 0, Color.GRAY));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        mainList.add(panel, gbc, 0);

        validate();
        repaint();
    }

}

