package client.view.panes;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

public class OnlineUsersPane extends JPanel {
    private JPanel mainList;
    private HashMap<String, JPanel> onlineUsers = new HashMap<>();

    public OnlineUsersPane(){
        setLayout(new BorderLayout());

        mainList = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1;
        gbc.weighty = 1;
        mainList.add(new JPanel(), gbc);

        add(new JScrollPane(mainList));
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(100, 500);
    }

    public void addUser(String name){
        JPanel panel = new JPanel();
        panel.add(new JLabel(name));
        onlineUsers.put(name, panel);
        panel.setBorder(new MatteBorder(0, 0, 1, 0, Color.GRAY));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        mainList.add(panel, gbc, 0);

        validate();
        repaint();
    }

    public void removeUser(String name){
        mainList.remove(onlineUsers.remove(name));
        validate();
        repaint();
    }
}
