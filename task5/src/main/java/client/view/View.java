package client.view;
import client.listeners.ConnectListener;
import client.listeners.LogoutListener;
import client.listeners.SendMessageListener;
import client.events.*;
import client.myObserver.MyObserver;
import client.view.frames.ChatFrame;
import client.view.frames.ErrorWindow;
import client.view.frames.LoginFrame;
import common.Message;

public class View implements MyObserver {

    private LoginFrame loginFrame = new LoginFrame();
    private ChatFrame chatFrame = new ChatFrame();


    public View() {
    }

    public void startWindowShow(boolean value){
        loginFrame.setVisible(value);
    }

    public void setNewMessageListener(SendMessageListener sendMessageListener) {
        chatFrame.setNewMessageListener(sendMessageListener);
    }

    public void setConnectListener(ConnectListener connectListener) {
        loginFrame.setConnectListener(connectListener);
    }

    public void setLogoutListener(LogoutListener logoutListener) {
        chatFrame.setLogoutListener(logoutListener);
    }

    @Override
    public void update(BusyName event) {
        ErrorWindow errorWindow = new ErrorWindow("Name is busy. Write another one.", false);
        errorWindow.setVisible(true);
    }

    @Override
    public void update(NewMessage event) {
        chatFrame.addMessage(event.getMessage());
    }

    @Override
    public void update(SuccessLogin event) {
        loginFrame.setVisible(false);
        loginFrame.dispose();
        chatFrame.setVisible(true);
        chatFrame.addMessage(new Message("Server", "You have joined chat!"));
    }

    @Override
    public void update(UserJoin event) {
        chatFrame.addMessage(new Message("Server", event.getUserName() + " join chat"));
        chatFrame.addUser(event.getUserName());
    }

    @Override
    public void update(UserLeft event) {
        chatFrame.addMessage(new Message("Server", event.getUserName() + " left chat"));
        chatFrame.removeUser(event.getUserName());
    }

    @Override
    public void update(BadServerAddress event) {
        ErrorWindow errorWindow = new ErrorWindow("Bad server address. Write another one", false);
        errorWindow.setVisible(true);
    }

    @Override
    public void update(SocketProblem event) {
        ErrorWindow errorWindow = new ErrorWindow("Connection was broken.", true);
        errorWindow.setVisible(true);
    }

    @Override
    public void update(OldUserOnline event) {
        chatFrame.addUser(event.getUserName());
    }

}
