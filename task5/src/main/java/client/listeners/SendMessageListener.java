package client.listeners;

public interface SendMessageListener {
    void sendMessage(String message);
}
