package client.listeners;

public interface ConnectListener {
    void connect(String serverAddress, String name);
}
