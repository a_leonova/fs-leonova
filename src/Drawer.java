public interface Drawer {

    void fillCell(String number);
    void drawFirstRow();
    void rowEnd();
    void drawRowNumber(String rowNumber);
}
