public class TableFiller {

    private int tableSize;
    private Drawer drawer;

    public TableFiller(int tableSize, Drawer drawer){

        this.tableSize = tableSize;
        this.drawer = drawer;
    }

    public void fillTable(){
        drawer.drawFirstRow();
        for(int i = 1; i <= tableSize; ++i){
            drawer.drawRowNumber(String.valueOf(i));
            for(int j = 1; j <= tableSize; ++j){
                drawer.fillCell(String.valueOf(i * j));
            }
            drawer.rowEnd();
        }
    }



}
