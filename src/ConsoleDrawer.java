import java.util.Collections;

public class ConsoleDrawer implements Drawer {
    private static final String HORIZONTAL_SEPARATOR = "-";
    private static final char VERTICAL_SEPARATOR = '|';
    private static final String CROSS = "+";

    private int maxFirstColumnCellSize;
    private int maxUsualCellSize;
    private int rowSize;

    private StringBuilder separatorForTitle = new StringBuilder();
    private StringBuilder longLineSeparator = new StringBuilder();

    public ConsoleDrawer(int rowSize, int maxFirstColumnSize, int maxOtherColumnSize){
        this.maxFirstColumnCellSize = maxFirstColumnSize;
        this.maxUsualCellSize = maxOtherColumnSize;
        this.rowSize = rowSize;

        createSeparator(separatorForTitle, maxFirstColumnSize, HORIZONTAL_SEPARATOR);
        StringBuilder separatorForUsualCell = new StringBuilder();
        createSeparator(separatorForUsualCell, maxUsualCellSize, HORIZONTAL_SEPARATOR);
        createSeparator(longLineSeparator, rowSize, CROSS + separatorForUsualCell.toString());

    }

    @Override
    public void fillCell(String number) {
        System.out.print(VERTICAL_SEPARATOR);
        drawNumber(number, maxUsualCellSize);
    }


    @Override
    public void rowEnd(){
        System.out.println();
        drawSeparator();
        System.out.println();

    }

    @Override
    public void drawRowNumber(String rowNumber) {
        drawNumber(rowNumber, maxFirstColumnCellSize);
    }

    @Override
    public void drawFirstRow(){
        drawSpaces(maxFirstColumnCellSize);
        for(int i = 1; i <= rowSize; ++i){
            System.out.print(VERTICAL_SEPARATOR);
            drawNumber(String.valueOf(i), maxUsualCellSize);
        }
        System.out.println();
        drawSeparator();
        System.out.println();
    }

    private void drawNumber(String number, int margin){
        drawSpaces(margin - number.length());
        System.out.print(number);
    }

    private void drawSeparator() {
        System.out.print(separatorForTitle.toString());
        System.out.print(longLineSeparator.toString());
    }

    private void createSeparator(StringBuilder str, int size, String separator){
        str.append(String.join("", Collections.nCopies(size, separator)));
    }

    private void drawSpaces(int cnt){
        StringBuilder spaces =  new StringBuilder();
        spaces.append(String.join("", Collections.nCopies( cnt, " ")));
        System.out.print(spaces);
    }

}
