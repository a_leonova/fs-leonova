public class Main {
    public static final int FROM = 1;
    public static final int TO = 32;

    public static void main(String[] args) {

        int tableSize;
        try {
            tableSize = checkArgs(args);
        }catch (IllegalArgumentException e){
            System.err.println(e.getMessage());
            return;
        }

        Drawer drawer = new ConsoleDrawer(tableSize,
                countDigitsIntoNumber(tableSize),
                countDigitsIntoNumber(tableSize * tableSize));

        TableFiller table = new TableFiller(tableSize, drawer);

        table.fillTable();
    }

    private static int countDigitsIntoNumber(int number){
        return String.valueOf(number).length();
    }

    private static int checkArgs(String[] args) throws IllegalArgumentException{
        if(args.length != 1){
            throw new IllegalArgumentException("Invalid count of arguments. Write only one number from " + FROM + "to " + TO);
        }

        int tableSize;

        try{
            tableSize = Integer.parseInt(args[0]);
        } catch (NumberFormatException e){
            throw new IllegalArgumentException("Argument is not a number. Write only one number from " + FROM + "to " + TO);
        }

        if(tableSize < FROM || tableSize > TO){
            throw new IllegalArgumentException("Invalid value of argument. Write only one number from " + FROM + "to " + TO);
        }
        return tableSize;
    }
}
